import $ from 'jquery';

export default function (domain) {
    return $.get('/domain/suggest/' + domain);
}