import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap';
import 'uikit';
import 'uikit/dist/js/uikit-icons';
import checkDomainAvailability from './check_domain_availability';
import getSuggestedDomains from './get_suggested_domains';

window.$ = $;

$(document).ready(() => {

    /**
     * Checking domain name
     */
   const domainInput = $('#attach_domain_domainName');
   const checkDNS = $('#check_dns_config');
   const siteToAttach = $('#site_to_attach');
   const success = $('.success');
   const failure = $('.failure');

   const checkDomainToAttach = function(domain) {
       checkDNS.html('<span class="glyphicon glyphicon-refresh spinning"></span> Chargement...');
       checkDomainAvailability(domain).then(() => {
           domainInput.parent().removeClass('has-error');
           domainInput.parent().addClass('has-success');
           checkDNS.html('Vérifier la configuration');
           failure.addClass('hidden');
           siteToAttach.removeClass('hidden');
           success.removeClass('hidden');
       }, () => {
           siteToAttach.addClass('hidden');
           success.addClass('hidden');
           checkDNS.html('Vérifier la configuration');
           domainInput.parent().removeClass('has-success');
           domainInput.parent().addClass('has-error');
           failure.removeClass('hidden');
       });
   };

   if (domainInput.length) {
      $('#check_dns_config').on('click', () => {
         checkDomainToAttach(domainInput.prop('value'));
      });
       $(document).keypress((e) => {
           if (e.which === 13) {
               e.preventDefault();
               checkDomainToAttach(domainInput.prop('value'));
               return false;
           }
       });
   }

    /**
     * Getting the list of suggested domain names
     */
    const domainBuyInput = $('#get_domain_domainName');
    const inputGroup = domainBuyInput.parent('.input-group');
    const results = $('.results tbody');
    const viewSuggestions = $('#view-succestions');

    const showErrorMessage = function (error, message) {
        inputGroup.addClass('has-error').parent().after('<div class="col-sm-offset-2 col-sm-8 alert alert-danger response-error"><b>' + error + '</b><p>' + message + '</p></div>');
    };

    const fetchDomains = function () {
        $('.response-error').remove();

        if (!domainBuyInput.get(0).validity.valid) {
            showErrorMessage("Erreur de format", "Merci d'entrer un nom de domaine correspondant au format suivant : seuls les caractères alphanumériques et les tirets sont autorisés, mais pas au début ni à la fin de votre nom de domaine");
            return;
        }
        inputGroup.removeClass('has-error');
        $('.results').addClass('hidden');
        domainBuyInput.parent().parent().removeClass('has-error');
        viewSuggestions.html('<span class="glyphicon glyphicon-refresh spinning"></span> Chargement...');
        getSuggestedDomains(domainBuyInput.val()).then((response) => {
            results.html('');
            for(let domain of response) {
                let available = domain.available === 'available';
                let price = available ? domain.prices[0].unit_price[0].price + '€/an' : 'Non disponible';
                let rowClass = available ? '' : 'danger';
                let chooseDomain = available ? '<button class="btn btn-default choose-domain"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Acheter</button>' : '<span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span>';
                results.append('<tr class="'+ rowClass +'"><td>' + domain.extension + '</td><td>' + price + '</td><td>' + chooseDomain + '</td></tr>')
            }
            $('.results').removeClass('hidden');
            return false;
        }, (response) => {
            showErrorMessage(response.responseJSON.error, response.responseJSON.message);
        }).then(() => {
            viewSuggestions.html('Voir les suggestions');
        });
    };

    if (domainBuyInput.length) {
        viewSuggestions.on('click', fetchDomains);
        $(document).keypress((e) => {
            if (e.which === 13) {
                e.preventDefault();
                fetchDomains();
                return false;
            }
        });

        $('body').on('click', '.choose-domain', (e) => {
            let domain = $(e.target).parent().parent().find('td').first().text();
            domainBuyInput.val(domain);
            // e.preventDefault();
            // return false;
        });
    }

    function updateUserType(target) {
        if ($(target).val() > 1) {
            $('#legalnamegroup').removeClass('hidden');
        } else {
            $('#legalnamegroup').addClass('hidden');
        }
    }

    if ($('#profile').length || $('#card-add-user-mangopay').length) {
        updateUserType($('#update_user_type input'));
        $('#update_user_type input').change((e) => {
           updateUserType(e.target)
        });
    }

    function calculateSuffix(siteType) {
        if (siteType.toLowerCase() === 'wiki') {
            return 'wiki';
        }
        return 'site';
    }

    function updateResultAddress(subdomain, target) {
        console.log('subdomain changed');
        if (subdomain.length > 0) {
            target.removeClass('hidden');
            target.addClass('alert-success');
            console.log('active text');
            target.html(`L'adresse de votre site sera : <b>https://${subdomain}.frama.${calculateSuffix(target.data('site-type'))}</b>`);
        } else {
            target.addClass('hidden');
        }
    }

    function checkSubdomainAvailablility(subdomain, target) {
        fetch('/site/available?domain=' + subdomain + '&type=' + target.data('site-type'), { credentials: 'same-origin'}).then((response) => {
            if (!response.ok) {
                response.json().then((response) => {
                    target.removeClass('hidden');
                    target.addClass('alert-danger');
                    target.html(response.message);
                });
            } else {
                target.removeClass('alert-danger');
            }
        })
    }

    if($('.new-site').length) {
        const subdomain = $('input#blog_subdomain, input#wiki_subdomain, input#single_page_subdomain');
        const fulldomain = $('.address-result');
        subdomain.bind('input', () => {
            if (subdomain[0].checkValidity()) {
                subdomain.parent().removeClass('has-error');
                updateResultAddress(subdomain.val(), fulldomain);
                checkSubdomainAvailablility(subdomain.val(), fulldomain)
            } else {
                subdomain.parent().addClass('has-error');
            }
            if (subdomain.val().length === 0) {
                fulldomain.addClass('hidden');
            }
        });
        subdomain.on('change', () => {
            if (subdomain[0].checkValidity()) {
                subdomain.parent().removeClass('has-error');
                updateResultAddress(subdomain.val(), fulldomain);
                checkSubdomainAvailablility(subdomain.val(), fulldomain)
            } else {
                subdomain.parent().addClass('has-error');
            }
            if (subdomain.val().length === 0) {
                fulldomain.addClass('hidden');
            }
        });
        subdomain.on('invalid', () => {
            subdomain.parent().addClass('has-error');
        });

        // For grav only
        if ($('.new-site.blog').length) {
            // Site modules
            const blogHomepageSelector = $('#blog_homepage');
            let homepageChoice = [];
            let modulesLabels = [];
            const blogHomepages = document.getElementById('blog_homepage').options;
            for (let option of blogHomepages) {
                modulesLabels[option.value] = option.text;
            }
            let checkboxesValues = [];
            const checkboxes = $('input[name*="siteModules"]');
            checkboxes.change((checkbox) => {
                if (checkbox.target.checked) {
                    homepageChoice[checkbox.target.value] = $(checkbox.target).parent().text();
                } else {
                    homepageChoice.splice(checkbox.target.value);
                }
                updateHomePageChoice();
            });

            function updateHomePageChoice() {
                blogHomepageSelector.empty();
                homepageChoice.forEach((option, i) => {
                    console.log(option);
                    console.log(i);
                    let opt = document.createElement('option');
                    opt.text = option;
                    opt.value = i;
                    blogHomepageSelector.get(0).add(opt, null);
                });
                if (homepageChoice.length > 2) {
                    blogHomepageSelector.parent().parent().removeClass('hidden');
                } else {
                    blogHomepageSelector.parent().parent().addClass('hidden');
                }
            }
        }
    }

    $('.passwordToCheck').on('change', () => {
        const first = $('.passwordToCheck.first');
        const second = $('.passwordToCheck.second');

        if (first.val().length < 8) {
            $('.passwordToCheck').parent().addClass('has-error');
            $('#short').removeClass('hidden');
        } else {
            $('.passwordToCheck').parent().removeClass('has-error');
            $('#short').addClass('hidden');
        }

        if (second.val() !== '' && first.val() !== second.val()) {
            $('.passwordToCheck').parent().addClass('has-error');
            $('#different').removeClass('hidden');
        } else {
            $('.passwordToCheck').parent().removeClass('has-error');
            $('#different').addClass('hidden');
        }
    });
});
