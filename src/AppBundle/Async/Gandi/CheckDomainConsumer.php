<?php

namespace AppBundle\Async\Gandi;

use AppBundle\Entity\Command;
use AppBundle\Entity\CommandDomain;
use AppBundle\Entity\Domain;
use AppBundle\Helper\DomainFactory;
use AppBundle\Service\Domain\RegisterDomainService;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class CheckDomainConsumer implements ConsumerInterface
{

    /**
     * @var RegisterDomainService
     */
    private $domainService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager, RegisterDomainService $domainService, DomainFactory $domainFactory, SerializerInterface $serializer)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->domainService = $domainService;
        $this->domainFactory = $domainFactory;
        $this->serializer = $serializer;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $this->logger->debug('Message received is', [$msg->getBody()]);
        /** @var CommandDomain $commandDomain */
        $commandDomain = $this->serializer->deserialize($msg->getBody(), CommandDomain::class, 'json');

        if (!($commandDomain instanceof CommandDomain)) {
            $this->logger->error('Wrong message for CheckDomainConsumer');
            return false;
        }
        $domain = $commandDomain->getDomain();
        $command = $commandDomain->getDomain();

        $this->logger->info("Getting create_domain operation to check if it's finished", [$domain->getId()]);
        $domainName = $domain->getDomainName();
        $operation = $this->domainFactory->getOperationsList([
                                                                 'domain' => $domainName,
                                                                 'type' => 'domain_create',
                                                             ])[0];

        if ($operation['step'] === 'DONE') {
            $command->setStatus(Command::STATUS_FINISHED_PROCESSED);
            $domain->setStatus(Domain::DOMAIN_GANDI_CREATED);

            $this->entityManager->persist($command);
            $this->entityManager->persist($domain);
            $this->entityManager->flush();

            $this->logger->info('Domain registration for ' . $domainName . ' is done');

            return true;
        } elseif (in_array($operation['step'], ['ERROR', 'CANCEL', 'SUPPORT'])) {
            $this->logger->error('Domain registration ended with ' . $operation['step'], [$operation]);
            return false;
        }
        $this->logger->info('Domain registration for ' . $domainName. ' is still pending');
        return false;
    }
}
