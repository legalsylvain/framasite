<?php

namespace AppBundle\Exception\APIException\Release;

use AppBundle\Exception\APIException\ContactException;

class ReleaseContactException extends ContactException
{

    /**
     * @var string
     */
    private $gandiId;

    /**
     * ContactException constructor.
     * @param string $message
     * @param string $gandiId
     */
    public function __construct(string $message, string $gandiId)
    {
        parent::__construct($message);
        $this->gandiId = $gandiId;
    }

    /**
     * @return string
     */
    public function getGandiId()
    {
        return $this->gandiId;
    }

    /**
     * @param string $gandiId
     * @return ContactException
     */
    public function setGandiId(string $gandiId): ContactException
    {
        $this->gandiId = $gandiId;
        return $this;
    }
}
