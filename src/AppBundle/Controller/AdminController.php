<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Domain;
use AppBundle\Entity\Payment\BankAccount;
use AppBundle\Entity\Zone\Record;
use AppBundle\Entity\Zone\Version;
use AppBundle\Entity\Zone\Zone;
use AppBundle\Exception\APIException;
use AppBundle\Form\Payment\NewBankAccountType;
use AppBundle\Form\Zone\RecordType;
use AppBundle\Form\Zone\ZoneType;
use AppBundle\Helper\DomainFactory;
use AppBundle\Helper\EmailFactory;
use AppBundle\Helper\PaymentFactory;
use AppBundle\Helper\ZoneFactory;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Narno\Gandi\Api as GandiAPI;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var ZoneFactory
     */
    private $zoneFactory;

    /**
     * @var PaymentFactory
     */
    private $paymentFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AdminController constructor.
     * @param LoggerInterface $logger
     * @param DomainFactory $domainFactory
     * @param ZoneFactory $zoneFactory
     * @param PaymentFactory $paymentFactory
     */
    public function __construct(LoggerInterface $logger, DomainFactory $domainFactory, ZoneFactory $zoneFactory, PaymentFactory $paymentFactory)
    {
        $this->logger = $logger;
        $this->domainFactory = $domainFactory;
        $this->zoneFactory = $zoneFactory;
        $this->paymentFactory = $paymentFactory;
    }

    /**
     * @Route("/", name="admin-index")
     */
    public function adminIndexAction()
    {
        return $this->render(
            'default/admin/index.html.twig',
            ['stripe_mode' => $this->getParameter(
                'stripe_api_mode'
            ), 'gandi_mode' => $this->getParameter('app.gandi.api.mode')]
        );
    }

    /**
     * @Route("/contact", name="admin-list-contacts")
     */
    public function indexContactsAction(): Response
    {
        try {
            $api = new GandiAPI($this->getParameter('app.gandi.api.mode') !== 'prod');
            $this->logger->debug(
                'Mode is ' . $this->getParameter('app.gandi.api.mode') !== 'prod' ? 'test' : 'prod'
            );
            // set API key
            $apiKey = $this->getParameter('app.gandi.api_key.test');
            if ($this->getParameter('app.gandi.api.mode') === 'prod') {
                $apiKey = $this->getParameter('app.gandi.api_key.prod');
            }
            $contacts = $api->contact->list([$apiKey]);

            return $this->render(
                'default/admin/contacts/index.html.twig',
                [
                    'contacts' => $contacts,
                ]
            );
        } catch (\RuntimeException $e) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('flashes.admin.contacts.api_error', ['%error%' => $e->getMessage()], 'messages')
            );
            return $this->redirectToRoute('admin-index');
        }
    }

    /**
     * @Route("/domain", name="admin-list-domain")
     */
    public function indexDomainsAction()
    {
        $domainRepository = $this->getDoctrine()->getRepository('AppBundle:Domain');
        $domains = $domainRepository->findAll();
        return $this->render(
            'default/admin/domains/index.html.twig',
            [
                'domains' => $domains,
            ]
        );
    }

    /**
     * @Route("/domain/{domain}", name="admin-view-domain")
     *
     * @param Domain $domain
     * @param EmailFactory $emailFactory
     * @return Response
     */
    public function adminViewDomainAction(Domain $domain, EmailFactory $emailFactory)
    {
        $nbMailBoxes = 'N/A';

        /**
         * If we have registered the domain, we can haz mailboxes !
         */
        if ($domain->isRegisteredByFrama()) {
            try {
                $nbMailBoxes = $emailFactory->getNbMailboxes($domain->getDomainName());
            } catch (APIException $e) {
                $this->get('session')->getFlashBag()->add(
                    'warning',
                    $this->get('translator')->trans('flashes.mailbox.count.unable', [], 'messages')
                );
            }
        }

        try {
            $api = new GandiAPI($this->getParameter('app.gandi.api.mode') !== 'prod');
            $this->logger->debug(
                'Mode is ' . $this->getParameter('app.gandi.api.mode') !== 'prod' ? 'test' : 'prod'
            );
            // set API key
            $apiKey = $this->getParameter('app.gandi.api_key.test');
            if ($this->getParameter('app.gandi.api.mode') === 'prod') {
                $apiKey = $this->getParameter('app.gandi.api_key.prod');
            }
            $domainData = $api->domain->info([$apiKey, $domain->getDomainName()]);
            foreach ($domainData['contacts'] as $contactKey => $contactData) {
                switch ($contactKey) {
                    case 'admin':
                        $domain->addContact(
                            'admin',
                            (new Contact())->setGandiId($contactData['handle'])->setContactType(
                                Contact::CONTACT_TYPE_ADMIN
                            )
                        );
                        break;
                    case 'bill':
                        $domain->addContact(
                            'bill',
                            (new Contact())->setGandiId($contactData['handle'])->setContactType(
                                Contact::CONTACT_TYPE_BILL
                            )
                        );
                        break;
                    case 'owner':
                        $domain->addContact(
                            'owner',
                            (new Contact())->setGandiId($contactData['handle'])->setContactType(
                                Contact::CONTACT_TYPE_OWNER
                            )
                        );
                        break;
                    case 'reseller':
                        $domain->addContact(
                            'reseller',
                            (new Contact())->setGandiId($contactData['handle'])->setContactType(
                                Contact::CONTACT_TYPE_RESELLER
                            )
                        );
                        break;
                    case 'tech':
                        $domain->addContact(
                            'tech',
                            (new Contact())->setGandiId($contactData['handle'])->setContactType(
                                Contact::CONTACT_TYPE_TECH
                            )
                        );
                        break;
                    default:
                        break;
                }
            }

            return $this->render(
                'default/admin/domains/view.html.twig',
                [
                    'domain' => $domain,
                    'zone' => null !== $domain->getDnsZone() ? $this->zoneFactory->getZoneInfo(
                        $domain->getDnsZone()
                    ) : null,
                    'ourGandiId' => $this->getParameter('app.gandi.api.mode') === 'prod' ? $this->getParameter(
                        'app.gandi.api.id.prod'
                    ) : $this->getParameter('app.gandi.api.id.test'),
                    'nbMailBoxes' => $nbMailBoxes,
                ]
            );
        } catch (\RuntimeException $e) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('flashes.admin.contacts.api_error', ['%error%' => $e->getMessage()], 'messages')
            );
            return $this->redirectToRoute('admin-index');
        }
    }

    /**
     * @Route("/zone", name="admin-list-zone")
     *
     * @param ZoneFactory $zoneFactory
     * @return Response
     */
    public function adminIndexDNSZoneAction(ZoneFactory $zoneFactory): Response
    {
        $zones = $zoneFactory->getZoneList();
        return $this->render(
            'default/admin/zone/index.html.twig',
            [
                'zones' => $zones,
            ]
        );
    }

    /**
     * @Route("/zone/{zone}", name="admin-info-zone", requirements={"zone": "\d+"})
     *
     * @param int $zone
     * @param ZoneFactory $zoneFactory
     * @return Response
     */
    public function adminViewDNSZoneAction(int $zone, ZoneFactory $zoneFactory): Response
    {
        $zone = $zoneFactory->getZoneInfo($zone);
        return $this->render(
            'default/admin/zone/view.html.twig',
            [
                'zone' => $zone,
            ]
        );
    }

    /**
     * @Route("/zone/create", name="admin-create-zone")
     *
     * @param Request $request
     * @return Response
     */
    public function adminNewDNSZoneAction(Request $request): Response
    {
        $zone = new Zone();
        $zoneForm = $this->createForm(ZoneType::class, $zone);
        $zoneForm->handleRequest($request);

        if ($zoneForm->isSubmitted() && $zoneForm->isValid()) {
            $zone = $this->zoneFactory->createZone($zone);

            $this->get('session')->getFlashBag()->add(
                'info',
                $this->get('translator')->trans(
                    'flashes.admin.zone.created',
                    ['%zoneName%' => $zone->getName()],
                    'messages'
                )
            );

            return $this->redirectToRoute('admin-info-zone', ['zone' => $zone->getId()]);
        }
        return $this->render(
            'default/admin/zone/new.html.twig',
            [
                'form' => $zoneForm->createView(),
            ]
        );
    }

    /**
     * @Route("/zone/{zone}/create", name="admin-create-version")
     *
     * @param int $zone
     * @return Response
     */
    public function adminNewDNSZoneVersionAction(int $zone): Response
    {
        $version = new Version(new Zone($zone));

        $version = $this->zoneFactory->createVersion($version);

        $this->get('session')->getFlashBag()->add(
            'info',
            $this->get('translator')->trans(
                'flashes.admin.zone.version.created',
                ['%version%' => $version->getId()],
                'messages'
            )
        );
        return $this->redirectToRoute('admin-info-zone', ['zone' => $zone]);
    }

    /**
     * @Route("/zone/{zone}/{version}", name="admin-view-version")
     *
     * @param int $zone
     * @param int $version
     * @return Response
     */
    public function adminViewDNSZoneVersionAction(int $zone, int $version): Response
    {
        $zoneObj = $this->zoneFactory->getZoneInfo($zone);
        $versionObj = new Version($zoneObj);
        $versionObj->setId($version);
        $version = $this->zoneFactory->getZoneVersionRecords($versionObj);
        return $this->render(
            'default/admin/zone/version/view.html.twig',
            [
                'version' => $version,
            ]
        );
    }

    /**
     * @Route("/zone/{zone}/{version}/delete", name="admin-delete-version")
     *
     * @param int $zone
     * @param int $version
     * @return Response
     */
    public function adminDeleteDNSZoneVersionAction(int $zone, int $version): Response
    {
        $versionObj = new Version(new Zone($zone));
        $versionObj->setId($version);
        $this->zoneFactory->deleteZoneVersion($versionObj);
        return $this->redirectToRoute('admin-info-zone', ['zone' => $zone]);
    }

    /**
     * @Route("/zone/{zone}/{version}/new", name="admin-add-record")
     *
     * @param Request $request
     * @param int $zone
     * @param int $version
     * @return Response
     */
    public function adminAddDNSRecordAction(Request $request, int $zone, int $version): Response
    {
        $zoneObj = new Zone($zone);
        $versionObj = new Version($zoneObj);
        $versionObj->setId($version);
        $record = new Record();
        $zoneForm = $this->createForm(RecordType::class, $record);
        $zoneForm->handleRequest($request);

        if ($zoneForm->isSubmitted() && $zoneForm->isValid()) {
            $this->zoneFactory->addRecord($record, $versionObj);

            $this->get('session')->getFlashBag()->add(
                'info',
                $this->get('translator')->trans('flashes.admin.zone.record.added', [], 'messages')
            );

            return $this->redirectToRoute(
                'admin-view-version',
                ['zone' => $zoneObj->getId(), 'version' => $versionObj->getId()]
            );
        }
        return $this->render(
            'default/admin/zone/record/new.html.twig',
            [
                'form' => $zoneForm->createView(),
            ]
        );
    }

    /**
     * @Route("/zone/{zone}/{version}/active", name="admin-set-zone-version")
     *
     * @param int $zone
     * @param int $version
     * @return Response
     */
    public function adminSetZoneVersionActive(int $zone, int $version): Response
    {
        $versionObj = new Version(new Zone($zone));
        $versionObj->setId($version);
        try {
            $this->zoneFactory->setZoneVersionActive($versionObj);
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('flashes.admin.zone.version.set_active.success', [], 'messages')
            );
        } catch (APIException $e) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('flashes.admin.zone.version.set_active.error', [], 'messages')
            );
        }
        return $this->redirectToRoute('admin-info-zone', ['zone' => $zone]);
    }

    /**
     * @Route("/payment/list", name="payments-list")
     * @return Response
     */
    public function listPaymentsAction(): Response
    {
        return new JsonResponse($this->paymentFactory->listPaymentsToWallet());
    }

    /**
     * @Route("/operation", name="admin-operation-list")
     *
     * @param Request $request
     * @return Response
     */
    public function operationIndexAction(Request $request): Response
    {
        try {
            $operations = $this->domainFactory->getOperationsList();
            return $this->render(
                'default/admin/operations/index.html.twig',
                [
                    'operations' => $operations,
                ]
            );
        } catch (APIException $e) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('flashes.admin.operation.list.error', [], 'messages')
            );
            $this->logger->error($e->getMessage());
            $referer = $request->headers->get('referer');

            return $referer === '' ? $this->redirectToRoute('admin-index') : $this->redirect($referer);
        }
    }

    /**
     * @Route("/operation/{operation}", name="admin-operation-view")
     * @param Request $request
     * @param $operation
     * @return RedirectResponse|Response
     */
    public function operationViewAction(Request $request, $operation)
    {
        try {
            $operation = $this->domainFactory->getOperation($operation);
            return $this->render(
                'default/admin/operations/view.html.twig',
                [
                    'operation' => $operation,
                ]
            );
        } catch (APIException $e) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('flashes.admin.operation.view.error', [], 'messages')
            );
            $referer = $request->headers->get('referer');

            return $referer === '' ? $this->redirectToRoute('admin-index') : $this->redirect($referer);
        }
    }

    /**
     * @Route("/operation/{operation}/relaunch", name="admin-operation-relaunch")
     *
     * @param Request $request
     * @param $operation
     * @return RedirectResponse|Response
     */
    public function operationRelaunchAction(Request $request, $operation)
    {
        try {
            $operation = $this->domainFactory->relaunchOperation($operation);
            return $this->render(
                'default/admin/operations/view.html.twig',
                [
                    'operation' => $operation,
                ]
            );
        } catch (APIException $e) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('flashes.admin.operation.view.error', [], 'messages')
            );
            $referer = $request->headers->get('referer');

            return $referer === '' ? $this->redirectToRoute('admin-index') : $this->redirect($referer);
        }
    }

    /**
     * @Route("/sites", name="admin-list-sites")
     */
    public function sitesListAction()
    {
        /** @var Registry $doctrine */
        $doctrine = $this->getDoctrine();
        return $this->render(
            'default/admin/sites/index.html.twig',
            [
                'sites' => $doctrine->getRepository('AppBundle:Blog\Blog')->findAll(),
                'wikis' => $doctrine->getRepository('AppBundle:Wiki\Wiki')->findAll()
            ]
        );
    }

    /**
     * @Route("/command/{page}", name="admin-list-commands", defaults={"page": 1})
     *
     * @return Response
     */
    public function commandListAction(int $page): Response
    {
        $commands = $this->getDoctrine()->getRepository('AppBundle:Command')->findAll();
        return $this->render(':default/admin/commands:index.html.twig', ['commands' => $commands]);
    }

    /**
     * @Route("/payment/{paymentId}", name="admin-view-payment")
     *
     * @param string $paymentId
     * @return Response
     */
    public function paymentViewAction(string $paymentId): Response
    {
        return $this->render(
            'default/admin/payments/view.html.twig',
            ['payment' => $this->paymentFactory->getPayment($paymentId)]
        );
    }
}
