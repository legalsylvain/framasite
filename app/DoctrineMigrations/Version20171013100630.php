<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add the first_name, last_name & country attributes for an user
 */
class Version20171013100630 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE framasite_user ADD country VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE framasite_user ADD last_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE framasite_user RENAME COLUMN name TO first_name');
        $this->addSql('ALTER TABLE cert_task ALTER task_data TYPE JSON');
        $this->addSql('ALTER TABLE cert_task ALTER task_data DROP DEFAULT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE framasite_user ADD name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE framasite_user DROP country');
        $this->addSql('ALTER TABLE framasite_user DROP first_name');
        $this->addSql('ALTER TABLE framasite_user DROP last_name');
        $this->addSql('ALTER TABLE cert_task ALTER task_data TYPE JSON');
        $this->addSql('ALTER TABLE cert_task ALTER task_data DROP DEFAULT');
    }
}
