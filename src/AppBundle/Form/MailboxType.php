<?php

namespace AppBundle\Form;

use AppBundle\Entity\Mailbox;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailboxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('login', TextType::class, [
                'label' => 'mailbox.login',
                'label_attr' => ['class' => 'col-sm-2'],
                'attr' => ['placeholder' => 'mailbox.new.login.placeholder'],
            ])
            ->add('password', PasswordType::class, [
                'label' => 'mailbox.password',
                'label_attr' => ['class' => 'col-sm-2'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'mailbox.new.create',
                'attr' => ['class' => 'btn btn-primary'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => Mailbox::class,
       ]);
    }
}
