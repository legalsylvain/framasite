<?php

namespace AppBundle\Form\Payment;

use AppBundle\Entity\Payment\MangoPayCard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cardNumber', TextType::class, [
            'label_attr' => ['class' => 'col-sm-4'],
            'attr' => ['placeholder' => '4706750000000009', 'autocomplete' => 'cc-number'],
            'label' => 'settings.cards.new.number',
            ])
            ->add('cardExpirationDate', TextType::class, [
                'label_attr' => ['class' => 'col-sm-4'],
                'attr' => ['placeholder' => date('my')],
                'label' => 'settings.cards.new.expiration_date',
            ])
            ->add('cardCvx', TextType::class, [
                'label_attr' => ['class' => 'col-sm-2'],
                'attr' => ['placeholder' => '123', 'autocomplete' => 'cc-csc'],
                'label' => 'settings.cards.new.cvv',
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success'],
                'label' => 'settings.cards.new.submit',
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   'data_class' => MangoPayCard::class,
                               ]);
    }

    public function getBlockPrefix()
    {
        return null;
    }
}
