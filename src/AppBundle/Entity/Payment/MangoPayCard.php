<?php

namespace AppBundle\Entity\Payment;

class MangoPayCard
{

    /**
     * @var string
     */
    private $cardNumber;

    /**
     * @var string
     */
    private $cardExpirationDate;

    /**
     * @var string
     */
    private $cardCvx;

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     * @return MangoPayCard
     */
    public function setCardNumber(string $cardNumber): MangoPayCard
    {
        $this->cardNumber = $cardNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardExpirationDate()
    {
        return $this->cardExpirationDate;
    }

    /**
     * @param string $cardExpirationDate
     * @return MangoPayCard
     */
    public function setCardExpirationDate(string $cardExpirationDate): MangoPayCard
    {
        $this->cardExpirationDate = $cardExpirationDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardCvx()
    {
        return $this->cardCvx;
    }

    /**
     * @param string $cardCvx
     * @return MangoPayCard
     */
    public function setCardCvx(string $cardCvx): MangoPayCard
    {
        $this->cardCvx = $cardCvx;
        return $this;
    }
}
