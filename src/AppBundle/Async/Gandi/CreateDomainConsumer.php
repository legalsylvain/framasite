<?php

namespace AppBundle\Async\Gandi;

use AppBundle\Entity\CommandDomain;
use AppBundle\Exception\APIException;
use AppBundle\Service\Domain\RegisterDomainService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use JMS\Serializer\Serializer;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class CreateDomainConsumer implements ConsumerInterface
{

    /**
     * @var RegisterDomainService
     */
    private $domainService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager, RegisterDomainService $domainService, Serializer $serializer)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->domainService = $domainService;

        $this->serializer = $serializer;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $this->logger->debug("message received : ", [$msg->getBody()]);
        $commandDomain = $this->serializer->deserialize($msg->getBody(), CommandDomain::class, 'json');
        /** @var CommandDomain $commandDomain */

        if (!$commandDomain instanceof CommandDomain) {
            return false;
        }

        try {
            $this->entityManager->flush(); // TODO : remove test
            $this->domainService->processCommandDomain($commandDomain);
            $this->logger->info('Processed command domain');
            return true;
        } catch (APIException $e) {
            $this->logger->error('Issue with API Domain : ' . $e->getMessage());
            return false;
        } catch (EntityNotFoundException $e) {
            $this->logger->critical('Entity not found : ' . $e->getMessage());
            return true;
        }
    }
}
