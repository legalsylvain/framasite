<?php

namespace AppBundle\Form;

use AppBundle\Entity\Domain;
use AppBundle\Entity\Site;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AttachDomainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('domainName', TextType::class, [
                'required' => true,
                'label' => 'domain.attach.domain_name',
                'label_attr' => ['class' => 'col-sm-3'],
                ])
            ->add('site', EntityType::class, [
                'class' => Site::class,
                'attr' => ['class' => 'input-lg'],
                'choices' => $options['sites'],
                'label' => 'domain.attach.site',
                'label_attr' => ['class' => 'col-sm-3'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'domain.attach.save',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Domain::class,
            'sites' => null,
        ]);
    }
}
