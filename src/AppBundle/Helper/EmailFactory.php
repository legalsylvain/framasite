<?php


namespace AppBundle\Helper;

use AppBundle\Exception\APIException;
use Narno\Gandi\Api as GandiAPI;
use Psr\Log\LoggerInterface;

class EmailFactory
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var GandiAPI
     */
    private $api;

    /**
     * DomainFactory constructor.
     * @param LoggerInterface $logger
     * @param $apiMode
     * @param $apiKeyProd
     * @param $apiKeyTest
     */
    public function __construct(LoggerInterface $logger, $apiMode, $apiKeyProd, $apiKeyTest)
    {
        $this->logger = $logger;
        $this->logger->debug("Connecting to BuyDomains API in " . $apiMode !== 'prod' ? 'test' : 'prod' . "mode");
        $this->api = new GandiAPI($apiMode !== 'prod');
        // set API key
        $this->apiKey = $apiKeyTest;
        if ($apiMode === 'prod') {
            $this->apiKey = $apiKeyProd;
        }
    }

    /**
     * @param $domainName
     * @return int
     * @throws APIException
     */
    public function getNbMailboxes($domainName): int
    {
        try {
            return $this->api->domain->mailbox->count([$this->apiKey, $domainName]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param string $domainName
     * @param string $login
     * @param null|string $password
     * @param null|string $emailFallback
     * @return array
     * @throws APIException
     */
    public function createMailbox(string $domainName, string $login, string $password = null, string $emailFallback = null): array
    {
        $params = [];
        if (null != $password) {
            $params['password'] = $password;
        }
        if (null != $emailFallback) {
            $params['fallback_email'] = $emailFallback;
        }

        try {
            return $this->api->domain->mailbox->create([$this->apiKey, $domainName, $login, $params]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param string $domainName
     * @param string $login
     * @return int
     * @throws APIException
     */
    public function deleteMailbox(string $domainName, string $login): int
    {
        try {
            $returnCode = $this->api->domain->mailbox->delete([$this->apiKey, $domainName, $login]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
        if ($returnCode !== 1) {
            throw new APIException("Return code was not 1");
        }
        return $returnCode;
    }

    /**
     * @param string $domainName
     * @param string $login
     * @return array
     * @throws APIException
     */
    public function infoMailbox(string $domainName, string $login): array
    {
        try {
            return $this->api->domain->mailbox->info([$this->apiKey, $domainName, $login]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param string $domainname
     * @return array
     * @throws APIException
     */
    public function listMailboxes(string $domainname): array
    {
        try {
            return $this->api->domain->mailbox->list([$this->apiKey, $domainname]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param string $domainName
     * @param string $login
     * @return array
     * @throws APIException
     */
    public function purgeMailboxe(string $domainName, string $login): array
    {
        try {
            return $this->api->domain->mailbox->list([$this->apiKey, $domainName, $login]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param string $domainName
     * @param string $login
     * @param string $password
     * @return array
     * @throws APIException
     */
    public function updateMailbox(string $domainName, string $login, string $password): array
    {
        try {
            return $this->api->domain->mailbox->update([$this->apiKey, $domainName, $login, ['password' => $password]]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }

    /**
     * @param string $domainName
     * @param string $login
     * @param array $aliases
     * @return array
     * @throws APIException
     */
    public function setAliases(string $domainName, string $login, array $aliases): array
    {
        try {
            return $this->api->domain->mailbox->alias->set([$this->apiKey, $domainName, $login, $aliases]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }
}
