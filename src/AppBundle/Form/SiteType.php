<?php

namespace AppBundle\Form;

use AppBundle\Entity\Site;
use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteType extends AbstractType
{
    /** @var User */
    protected $user;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subdomain', TextType::class, [
                'label' => 'site.new.subdomain.title',
                'attr' => ['placeholder' => 'site.new.subdomain.placeholder', 'maxlength' => 25, 'minlength' => 2, 'pattern' => '^[a-z0-9][-a-z0-9]{0,23}[a-z0-9]$'],
                'label_attr' => ['class' => 'col-sm-2'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'site.save',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Site::class,
        ]);
    }
}
