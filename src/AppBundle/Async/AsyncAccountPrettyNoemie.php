<?php

namespace AppBundle\Async;

use AppBundle\Entity\SinglePage\SinglePage;
use AppBundle\Entity\SinglePage\SinglePageUser;
use AppBundle\Exception\SiteException\SiteCreationException;
use AppBundle\Exception\SiteException\SiteDeletionException;
use AppBundle\Exception\SiteException\SubDomainFolderExistsException;
use AppBundle\Exception\SiteException\UserSiteException\UserSiteDeletionException;
use AppBundle\Exception\SiteUserExistsException;
use AppBundle\Exception\SubDomainUnavailableException;
use Symfony\Component\Filesystem\Filesystem;

class AsyncAccountPrettyNoemie extends AbstractMakeAccount implements AsyncAccountInterface
{
    /**
     * @var SinglePage
     */
    protected $site;

    /**
     * @var SinglePageUser
     */
    protected $siteUser;

    /**
     * Execute the async job
     *
     * @throws SubDomainUnavailableException
     * @throws SiteUserExistsException
     */
    public function make()
    {
        try {
            $this->logger->info(
                'Preparing to create site subdomain ' . $this->site->getSubdomain(
                ) . 'and account ' . $this->siteUser->getUsername() . ' for user ' . $this->user->getUsername() . '!'
            );
            $this->checkSubdomain();
            $this->makeSubdomain();

            $this->makeSiteConfig();
            $this->makeAccount();

            $this->logger->info(
                'Created subdomain ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!'
            );
        } catch (SubDomainFolderExistsException $e) {
            $this->logger->error("The subdomain " . $e->getFolder() . " already exists. Aborting");
            throw new SubDomainUnavailableException(SubDomainUnavailableException::REASON_TAKEN, $e->getMessage(), $this->user);
        }
    }

    /**
     * Check that subdomain actually exists
     *
     * @param string|null $subdomain
     * @return bool
     * @throws SubDomainFolderExistsException
     */
    public function checkSubdomain(string $subdomain = null)
    {
        $this->logger->info('Checking subdomain ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        $subdomainToCheck = null != $subdomain ? $subdomain : $this->site->getSubdomain();
        $fs = new Filesystem();
        $accountPath = $this->accountsPath . '/' . strtolower($subdomainToCheck);
        $this->logger->debug('Checked ' . $this->accountsPath . '/' . strtolower($subdomainToCheck) . ' path');
        if ($fs->exists($accountPath)) {
            throw new SubDomainFolderExistsException($accountPath, "Folder already exists");
        }
        return true;
    }

    /**
     * Create the subdomain and all necessary files
     */
    public function makeSubdomain()
    {
        $this->logger->info('Preparing subdomain creation ' . $this->site->getSubdomain() . '.frama.site for user ' . $this->user->getUsername() . '!');
        $fs = new Filesystem();
        $accountPath = $this->accountsPath . '/' . strtolower($this->site->getSubdomain() . '.frama.site');
        $fs->mkdir($accountPath);


        $this->logger->info('Created subdomain ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
    }

    public function makeSiteConfig()
    {
        $config = [
            "siteLabel" => $this->site->getSiteName(),
            "siteDomain" => $this->site->getSubdomain(),
            "siteLang" => $this->siteUser->getLocale(),
            "siteDescription" => $this->site->getSiteDescription(),
            "siteKeywords" => $this->site->getSiteKeywords()
        ];
        $data = json_encode($config, JSON_PRETTY_PRINT);
        file_put_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.site/config.json', $data);
        $this->logger->info('Created config for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
    }

    /**
     * @param string|null $username
     * @param string|null $subdomain
     * @return bool
     * @throws SiteUserExistsException
     */
    public function checkAccount(string $username = null, string $subdomain = null)
    {
        $usernameToCheck = null != $username ? $username : $this->siteUser->getUsername();
        $subdomainToCheck = null != $subdomain ? $subdomain : $this->site->getSubdomain();
        $this->logger->info('Checking account ' . $usernameToCheck . ' for site ' . $subdomainToCheck . ' for user ' . $this->user->getUsername() . '!');
        $config = json_decode(file_get_contents($this->accountsPath . '/' . $subdomainToCheck . '.frama.site/config.json'), true);
        if (array_key_exists('admins', $config)) {
            $this->logger->info("There's already one user");
            foreach ($config['admins'] as $user) {
                $this->logger->info("Checking against user " . $user['user_id']);
                if ($user['user_id'] === $usernameToCheck) {
                    throw new SiteUserExistsException($subdomainToCheck, $usernameToCheck, "User already exists !");
                }
            }
        }
        return true;
    }

    /**
     * Create an user account
     *
     * @throws SiteUserExistsException
     */
    public function makeAccount()
    {
        $this->logger->info('Creating account ' . $this->siteUser->getUsername() . ' for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        $this->checkAccount();
        $config = json_decode(file_get_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.site/config.json'), true);

        $config['admins'][] = ['user_id' => $this->siteUser->getUsername(), 'password' => password_hash($this->siteUser->getPassword(), PASSWORD_DEFAULT)];

        $data = json_encode($config, JSON_PRETTY_PRINT);
        file_put_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.site/config.json', $data);


        $this->logger->info('Created account ' . $this->siteUser->getUsername() . ' for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
    }

    /**
     * @throws SiteDeletionException
     */
    public function deleteSite()
    {
        $this->logger->info('Deleting site at subdomain ' . $this->site->getSubdomain() . '.frama.site' . '...');
        $fs = new Filesystem();
        $this->logger->info('Deleting site at subdomain ' . $this->site->getSubdomain() . '...');
        if (null == $this->site->getSubdomain() || $this->site->getSubdomain() == '') { // TODO : check against ../../ and such things
            throw new SiteDeletionException("Subdomain is empty, won't delete everything !");
        }
        $fs->remove($this->accountsPath . '/' . $this->site->getSubdomain());
    }

    /**
     * Deletes an user from a site
     *
     * @throws UserSiteDeletionException
     */
    public function deleteSiteUser()
    {
        $this->logger->info($this->user->getUsername() . " tried to delete account " . $this->siteUser->getUsername() . " for website " . $this->site->getSubdomain() . '.frama.site');

        $config = json_decode(file_get_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.site/config.json'), true);
        $foundUser = false;

        foreach ($config['admins'] as $key => $user) {
            if ($user['user_id'] === $this->siteUser->getUsername()) {
                $foundUser = true;
                unset($config['admins'][$key]);
            }
        }
        if ($foundUser) {
            $data = json_encode($config, JSON_PRETTY_PRINT);
            file_put_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.site/config.json', $data);
            return true;
        }
        throw new UserSiteDeletionException("This user doesn't exist. Strange");
    }

    /**
     * Edits an user from a site
     * @param string $oldUsername
     */
    public function editAccount(string $oldUsername)
    {
        $config = json_decode(file_get_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.site/config.json'), true);

        foreach ($config['admins'] as $key => $user) {
            if ($user['user_id'] === $oldUsername) {
                unset($config['admins'][$key]);
                $config['admins'][] = ['user_id' => $this->siteUser->getUsername(), 'password' => password_hash($this->siteUser->getPassword(), PASSWORD_DEFAULT)];
            }
        }

        $data = json_encode($config, JSON_PRETTY_PRINT);
        file_put_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '.frama.site/config.json', $data);
    }

    /**
     * @param string $source
     * @param string $link
     */
    public function linkDomainFolder(string $source, string $link)
    {
        $this->logger->info('Symlinking ' . $this->accountsPath . '/' . $source . '.frama.site' . ' and ' . $this->accountsPath . '/' . $link);
        $sourcePath = $this->accountsPath . '/' . $source . '.frama.site';
        $targetPath = $this->accountsPath . '/' . $link;
        Tools::relativeSymlink($sourcePath, $targetPath);
    }

    /**
     * @param string $domain
     */
    public function unlinkDomainFolder(string $domain)
    {
        $fs = new Filesystem();
        $this->logger->info('Unlinking ' . $this->accountsPath . '/' . $domain);
        $fs->remove($this->accountsPath . '/' . $domain);
    }
}
