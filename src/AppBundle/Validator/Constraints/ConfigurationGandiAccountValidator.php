<?php

namespace AppBundle\Validator\Constraints;

use Narno\Gandi\Exception\RuntimeException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Narno\Gandi\Api as GandiAPI;

class ConfigurationGandiAccountValidator extends ConstraintValidator
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var GandiAPI
     */
    private $api;

    /**
     * ConfigurationDomainValidator constructor.
     * @param LoggerInterface $logger
     * @param $apiMode
     * @param $apiKeyProd
     * @param $apiKeyTest
     */
    public function __construct(LoggerInterface $logger, $apiMode, $apiKeyProd, $apiKeyTest)
    {
        $this->api = new GandiAPI($apiMode !== 'prod');
        // set API key
        $this->apiKey = $apiKeyTest;
        if ($apiMode === 'prod') {
            $this->apiKey = $apiKeyProd;
        }
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param string $gandiId
     * @param Constraint $constraint
     */
    public function validate($gandiId, Constraint $constraint)
    {
        try {
            $contact = $this->api->contact->info([$this->apiKey, $gandiId]);
        } catch (RuntimeException $e) {
            if (strpos($e->getMessage(), 'CAUSE_BADPARAMETER') !== false) {
                $this->context->buildViolation($constraint->incorrectGandiAccountMessage)
                    ->addViolation();
            }
            if (strpos($e->getMessage(), 'CAUSE_NOTFOUND') !== false) {
                $this->context->buildViolation($constraint->notFoundGandiAccountMessage)
                    ->addViolation();
            }
        }
    }
}
