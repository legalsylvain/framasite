<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintValidDomainConfiguration extends Constraint
{
    public $incorrectDomainMessage = 'domain.incorrect';
    public $missingDomainMessage = 'domain.missing';
    public $unavailableDomainName = 'domain.unavailable';

    public $domainWithoutExtension = 'domain.extension.missing';
    public $domainWithExtensionNotSupported = 'domain.extension.not_supported';

    public function validatedBy()
    {
        return ConfigurationDomainValidator::class;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
