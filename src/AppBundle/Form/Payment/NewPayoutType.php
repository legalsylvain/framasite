<?php

namespace AppBundle\Form\Payment;

use AppBundle\Entity\Payment\MangoPayCard;
use AppBundle\Entity\Payment\MangoPayPayout;
use MangoPay\BankAccount;
use MangoPay\Wallet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewPayoutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $wallets = $options['wallets'];
        $bankAccounts = $options['bankAccounts'];
        $builder->add('debitedWalletId', ChoiceType::class, [
            'choices' => $wallets,
            'multiple' => false,
            'expanded' => false,
            'label_attr' => ['class' => 'col-sm-3'],
        ])
            ->add('bankAccountId', ChoiceType::class, [
                'choices' => $bankAccounts,
                'multiple' => false,
                'expanded' => false,
                'label_attr' => ['class' => 'col-sm-3'],
            ])
            ->add('debitedFunds', MoneyType::class, [
                'label_attr' => ['class' => 'col-sm-3'],
            ])
            ->add('reference', TextType::class, [
                'required' => false,
                'label_attr' => ['class' => 'col-sm-3'],
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success'],
                'label' => 'settings.cards.new.submit',
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                   'data_class' => MangoPayPayout::class,
                                    'wallets' => null,
                                    'bankAccounts' => null,
                               ]);
    }

    public function getBlockPrefix()
    {
        return null;
    }
}
