<?php

namespace AppBundle\Entity\Blog;

use AppBundle\Entity\AbstractSiteUser;

class BlogUser extends AbstractSiteUser
{
    /**
     * BlogUser constructor.
     * @param Blog|null $site
     */
    public function __construct(Blog $site = null)
    {
        parent::__construct($site);
        $this->admin = true;
    }

    /**
     * @var string
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @var bool
     */
    protected $admin = true;

    /**
     * @var string
     */
    protected $fullName;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return BlogUser
     */
    public function setEmail($email): BlogUser
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->admin;
    }

    /**
     * @param bool $admin
     * @return BlogUser
     */
    public function setAdmin(bool $admin): BlogUser
    {
        $this->admin = $admin;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     * @return BlogUser
     */
    public function setFullName(string $fullName): BlogUser
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return BlogUser
     */
    public function setLocale(string $locale): BlogUser
    {
        $this->locale = $locale;
        return $this;
    }
}
