<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    /**
     * @var string
     * @Serializer\Type("string")
     *
     * @Assert\Email(
     *     checkHost = true,
     *     strict=true
     *     )
     * @Assert\NotBlank
     * @Groups({"domain_register"})
     */
    private $email;

    /**
     * @var string
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank
     * @Groups({"domain_register"})
     */
    private $password;

    /**
     * @var string
     * @Serializer\Type("string")
     *
     * @Assert\NotBlank
     * @Groups({"domain_register"})
     */
    private $givenName;

    /**
     * @var string
     * @Assert\NotBlank
     * @Serializer\Type("string")
     * @Groups({"domain_register"})
     */
    private $familyName;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Serializer\Type("string")
     * @Groups({"domain_register"})
     */
    private $streetAddress;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Serializer\Type("string")
     * @Groups({"domain_register"})
     */
    private $zipCode;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Serializer\Type("string")
     * @Groups({"domain_register"})
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\Country
     * @Assert\NotBlank
     * @Serializer\Type("string")
     * @Groups({"domain_register"})
     */
    private $country;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Serializer\Type("string")
     * @Groups({"domain_register"})
     */
    private $phone;

    /**
     * @var int
     *
     * @Assert\NotBlank
     * @Assert\Type("integer")
     * @Assert\Range(
     *     min = 0,
     *     max = 3
     * )
     * @Serializer\Type("int")
     * @Groups({"domain_register"})
     */
    private $type;

    const TYPE_PRIVATE = 0;
    const TYPE_COMPANY = 1;
    const TYPE_ASSOCIATION = 2;
    const TYPE_PUBLIC_BODY = 3;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Groups({"domain_register"})
     */
    private $orgName;

    /**
     * @var int
     *
     * @Assert\NotBlank
     * @Assert\Type("integer")
     * @Assert\Range(
     *  min=0,
     *  max=3
     * )
     * @Serializer\Type("int")
     * @Groups({"domain_register"})
     */
    private $contactType;

    const CONTACT_TYPE_OWNER = 0;
    const CONTACT_TYPE_ADMIN = 1;
    const CONTACT_TYPE_BILL = 2;
    const CONTACT_TYPE_TECH = 3;
    const CONTACT_TYPE_RESELLER = 4;

    /**
     * Actually the Handle
     * @var string
     * @Serializer\Type("string")
     * @Groups({"domain_register"})
     */
    private $gandiId;

    /**
     * @var integer
     * @Serializer\Type("int")
     * @Groups({"domain_register"})
     */
    private $id;

    /**
     * @var boolean
     * @Serializer\Type("bool")
     * @Groups({"domain_register"})
     */
    private $obfuscateInfo = true;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Groups({"domain_register"})
     */
    private $lang = 'fr';

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Contact
     */
    public function setEmail(string $email): Contact
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getGivenName()
    {
        return $this->givenName;
    }

    /**
     * @param string $givenName
     * @return Contact
     */
    public function setGivenName(string $givenName): Contact
    {
        $this->givenName = $givenName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFamilyName()
    {
        return $this->familyName;
    }

    /**
     * @param string $familyName
     * @return Contact
     */
    public function setFamilyName(string $familyName): Contact
    {
        $this->familyName = $familyName;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * @param string $streetAddress
     * @return Contact
     */
    public function setStreetAddress(string $streetAddress): Contact
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return Contact
     */
    public function setZipCode(string $zipCode): Contact
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Contact
     */
    public function setCity(string $city): Contact
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return Contact
     */
    public function setCountry(string $country): Contact
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Contact
     */
    public function setPhone(string $phone): Contact
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Contact
     */
    public function setType(int $type): Contact
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Contact
     */
    public function setPassword($password): Contact
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int
     */
    public function getContactType()
    {
        return $this->contactType;
    }

    /**
     * @param int $contactType
     * @return Contact
     */
    public function setContactType(int $contactType): Contact
    {
        $this->contactType = $contactType;
        return $this;
    }

    /**
     * @return string
     */
    public function getGandiId()
    {
        return $this->gandiId;
    }

    /**
     * @param string $gandiId
     * @return Contact
     */
    public function setGandiId(string $gandiId = null): Contact
    {
        $this->gandiId = $gandiId;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Contact
     */
    public function setId(int $id = null): Contact
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrgName()
    {
        return $this->orgName;
    }

    /**
     * @param string $orgName
     * @return Contact
     */
    public function setOrgName(string $orgName): Contact
    {
        $this->orgName = $orgName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isObfuscateInfo(): bool
    {
        return $this->obfuscateInfo;
    }

    /**
     * @param bool $obfuscateInfo
     * @return Contact
     */
    public function setObfuscateInfo(bool $obfuscateInfo): Contact
    {
        $this->obfuscateInfo = $obfuscateInfo;
        return $this;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return Contact
     */
    public function setLang(string $lang): Contact
    {
        $this->lang = $lang;
        return $this;
    }
}
