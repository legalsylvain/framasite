<?php

namespace AppBundle\Entity\Payment;

use AppBundle\Entity\User;

class MangoPayUser
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var \DateTime
     */
    private $birthday;

    /**
     * @var string
     */
    private $country;

    /**
     * @var int
     */
    private $type = User::USER_TYPE_NATURAL;

    /**
     * @var string
     */
    private $legalName;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return MangoPayUser
     */
    public function setEmail(string $email): MangoPayUser
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return MangoPayUser
     */
    public function setFirstName(string $firstName): MangoPayUser
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return MangoPayUser
     */
    public function setLastName(string $lastName): MangoPayUser
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     * @return MangoPayUser
     */
    public function setBirthday(\DateTime $birthday): MangoPayUser
    {
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return MangoPayUser
     */
    public function setCountry(string $country): MangoPayUser
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return MangoPayUser
     */
    public function setType(int $type): MangoPayUser
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getLegalName()
    {
        return $this->legalName;
    }

    /**
     * @param string $legalName
     * @return MangoPayUser
     */
    public function setLegalName(string $legalName = null): MangoPayUser
    {
        $this->legalName = $legalName;
        return $this;
    }
}
