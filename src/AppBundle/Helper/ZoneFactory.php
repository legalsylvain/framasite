<?php

namespace AppBundle\Helper;

use AppBundle\Entity\Domain;
use AppBundle\Entity\Zone\Record;
use AppBundle\Entity\Zone\Version;
use AppBundle\Entity\Zone\Zone;
use AppBundle\Exception\APIException;
use Doctrine\Common\Collections\ArrayCollection;
use Narno\Gandi\Api as GandiAPI;
use Psr\Log\LoggerInterface;

class ZoneFactory
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var int
     */
    private $defaultZone;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var GandiAPI
     */
    private $api;

    /**
     * DomainFactory constructor.
     * @param LoggerInterface $logger
     * @param $defaultZone
     * @param $apiMode
     * @param $apiKeyProd
     * @param $apiKeyTest
     */
    public function __construct(LoggerInterface $logger, int $defaultZone, string $apiMode, string $apiKeyProd, string $apiKeyTest)
    {
        $this->logger = $logger;
        $this->defaultZone = $defaultZone;
        $this->logger->debug("Connecting to BuyDomains API in " . $apiMode !== 'prod' ? 'test' : 'prod' . "mode");
        $this->api = new GandiAPI($apiMode !== 'prod');
        // set API key
        $this->apiKey = $apiKeyTest;
        if ($apiMode === 'prod') {
            $this->apiKey = $apiKeyProd;
        }
    }

    /**
     * @param string|null $exclude
     * @return array
     */
    public function getZoneList(string $exclude = null): array
    {
        $params = [$this->apiKey];
        if (null != $exclude) {
            $params[] = ['!owner' => $exclude];
        }
        $zones = [];
        $zonesData = $this->api->domain->zone->list($params);
        foreach ($zonesData as $zonesDatum) {
            $zone = new Zone();
            $zone->setId($zonesDatum['id'])->setName($zonesDatum['name']);
            $zones[] = $zone;
        }
        return $zones;
    }

    /**
     * @param int $zoneId
     * @return Zone
     */
    public function getZoneInfo(int $zoneId): Zone
    {
        $zoneData = $this->api->domain->zone->info([$this->apiKey, $zoneId]);
        $zone = new Zone();
        $zone->setId($zoneData['id'])->setName($zoneData['name'])->setNbDomains($zoneData['domains'])->setVersions(new ArrayCollection($zoneData['versions']))->setCurrentVersion($zoneData['version']);
        return $zone;
    }

    /**
     * @param Zone $zone
     * @return Zone
     */
    public function createZone(Zone $zone): Zone
    {
        $zoneData = $this->api->domain->zone->create([$this->apiKey, ['name' => $zone->getName()]]);
        $zone->setId($zoneData['id'])->setNbDomains($zoneData['domains'])->setVersions(new ArrayCollection($zoneData['versions']))->setCurrentVersion($zoneData['version']);
        return $zone;
    }

    /**
     * @param Version $version
     * @return Version
     */
    public function createVersion(Version $version): Version
    {
        $rawData = [
            $this->apiKey,
            'zone_id' => $version->getZone()->getId()];
        if (null !== $version->getParent()) {
            $rawData['version_id'] = $version->getParent()->getId();
        }
        $zoneData = $this->api->domain->zone->version->new($rawData);
        $version->setId($zoneData);
        return $version;
    }

    /**
     * @param Version $version
     * @return Version
     */
    public function getZoneVersionRecords(Version $version): Version
    {
        $zoneData = $this->api->domain->zone->record->list([$this->apiKey, $version->getZone()->getId(), $version->getId()]);
        foreach ($zoneData as $zoneDatum) {
            $version->addRecord(new Record($zoneDatum['id'], $zoneDatum['name'], $zoneDatum['ttl'], $zoneDatum['type'], $zoneDatum['value']));
        }
        return $version;
    }

    /**
     * @param Record $record
     * @param Version $version
     * @return Record
     */
    public function addRecord(Record $record, Version $version): Record
    {
        $recordRawData = [
            'name' => $record->getName(),
            'type' => $record->getType(),
            'value' => $record->getValue(),
            'ttl' => $record->getTtl(),
        ];
        $recordData = $this->api->domain->zone->record->add([$this->apiKey, $version->getZone()->getId(), $version->getId(), $recordRawData]);
        $record->setId($recordData['id']);
        return $record;
    }

    /**
     * @param Version $version
     * @throws APIException
     */
    public function setZoneVersionActive(Version $version)
    {
        $result = $this->api->domain->zone->version->set([$this->apiKey, $version->getZone()->getId(), $version->getId()]);
        if ($result !== 1) {
            throw new APIException("Couldn't set Zone " . $version->getZone()->getId() . " to version " . $version->getId());
        }
    }

    /**
     * @param Version $version
     * @throws APIException
     */
    public function deleteZoneVersion(Version $version)
    {
        $result = $this->api->domain->zone->version->delete([$this->apiKey, $version->getZone()->getId(), $version->getId()]);
        if ($result !== 1) {
            throw new APIException("Couldn't delete Zone " . $version->getZone()->getId() . " version " . $version->getId());
        }
    }

    /**
     * @param Zone $zone
     * @param Domain $domain
     */
    public function setZoneForDomain(Zone $zone, Domain $domain)
    {
        $this->api->domain->zone->set([$this->apiKey, $domain->getDomainName(), $zone->getId()]);
    }

    /**
     * @param Zone $zone
     * @param int $versionId
     * @param null|string $zoneName
     * @return Zone
     */
    public function cloneZone(Zone $zone, int $versionId = 0, string $zoneName = null): Zone
    {
        $params = $zoneName !== null ? [$this->apiKey, $zone->getId(), $versionId, ['name' => $zoneName]] : [$this->apiKey, $zone->getId(), $versionId];
        $zoneData = $this->api->domain->zone->clone($params);

        return (new Zone($zoneData['id']))->setName($zoneData['name'])->setCurrentVersion($zoneData['version']);
    }

    /**
     * @return int
     */
    public function getDefaultZone()
    {
        return $this->defaultZone;
    }

    /**
     * @param Zone $zone
     * @return int
     * @throws APIException
     */
    public function countZoneVersions(Zone $zone): int
    {
        try {
            return $this->api->domain->zone->version->count([$this->apiKey, $zone->getId()]);
        } catch (\RuntimeException $e) {
            throw new APIException($e->getMessage());
        }
    }
}
