<?php

namespace AppBundle\Entity\Certificate;

class CertData
{
    const TYPE_SITE = 10;
    const TYPE_WIKI = 20;
    const TYPE_SINGLE = 30;

    const ACTION_CREATE = 10;
    const ACTION_REDUCE = 20; // Révocation d'un domaine parmi d'autre
    const ACTION_DELETE = 30; // delete du vhost & révocation certif
    /**
     * @var string
     */
    private $subdomain;

    /**
     * @var string
     */
    private $suffix;

    /**
     * @var int
     */
    private $type;

    /**
     * @var array
     */
    private $names = [];

    /**
     * @var int
     */
    private $action;

    /**
     * Id of next task to do
     * @var int
     */
    private $next;

    /**
     * @var string
     */
    private $userLocale;

    /**
     * @var string
     */
    private $userEmail;

    /**
     * @return string
     */
    public function getSubdomain(): string
    {
        return $this->subdomain;
    }

    /**
     * @param string $subdomain
     * @return CertData
     */
    public function setSubdomain(string $subdomain): CertData
    {
        $this->subdomain = $subdomain;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * @param string $suffix
     * @return CertData
     */
    public function setSuffix(string $suffix = null): CertData
    {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return CertData
     */
    public function setType(int $type): CertData
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param array $names
     * @return CertData
     */
    public function setNames(array $names = null): CertData
    {
        $this->names = $names;
        return $this;
    }

    /**
     * @param string $name
     * @return CertData
     */
    public function addName(string $name): CertData
    {
        $this->names[] = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getAction(): int
    {
        return $this->action;
    }

    /**
     * @param int $action
     * @return CertData
     */
    public function setAction(int $action = null): CertData
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return int
     */
    public function getNext()
    {
        return $this->next;
    }

    /**
     * @param int $next
     * @return CertData
     */
    public function setNext(int $next = null): CertData
    {
        $this->next = $next;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserLocale(): string
    {
        return $this->userLocale;
    }

    /**
     * @param string $userLocale
     * @return CertData
     */
    public function setUserLocale(string $userLocale): CertData
    {
        $this->userLocale = $userLocale;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @param string $userEmail
     * @return CertData
     */
    public function setUserEmail(string $userEmail): CertData
    {
        $this->userEmail = $userEmail;
        return $this;
    }
}
