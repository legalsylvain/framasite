<?php

namespace AppBundle\Controller;

use AppBundle\Async\AsyncAccountDoku;
use AppBundle\Async\AsyncAccountGrav;
use AppBundle\Async\AsyncAccountInterface;
use AppBundle\Async\AsyncAccountPrettyNoemie;
use AppBundle\Entity\AbstractSiteUser;
use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Blog\BlogUser;
use AppBundle\Entity\Certificate\CertData;
use AppBundle\Entity\Domain;
use AppBundle\Entity\SinglePage\SinglePage;
use AppBundle\Entity\SinglePage\SinglePageUser;
use AppBundle\Entity\Site;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Entity\Wiki\WikiUser;
use AppBundle\Exception\SiteException\SiteCreationException;
use AppBundle\Exception\SiteException\SiteDeletionException;
use AppBundle\Exception\SiteException\UnknownSiteType;
use AppBundle\Exception\SiteException\UserSiteException\UserSiteDeletionException;
use AppBundle\Exception\SiteUserExistsException;
use AppBundle\Exception\SubDomainUnavailableException;
use AppBundle\Form\Blog\BlogType;
use AppBundle\Form\Blog\BlogUserType;
use AppBundle\Form\DeleteSiteForm;
use AppBundle\Form\SinglePage\SinglePageType;
use AppBundle\Form\SinglePage\SinglePageUserType;
use AppBundle\Form\Wiki\WikiType;
use AppBundle\Form\Wiki\WikiUserType;
use AppBundle\Helper\CertTaskFactory;
use AppBundle\Helper\SiteFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SiteController extends Controller
{
    /**
     * @var AsyncAccountGrav
     */
    private $accountGrav;

    /**
     * @var AsyncAccountDoku
     */
    private $accountDoku;

    /**
     * @var AsyncAccountPrettyNoemie
     */
    private $accountPrettyNoemie;

    /**
     * @var CertTaskFactory
     */
    private $certTaskFactory;

    /**
     * @var SiteFactory
     */
    private $siteFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SiteController constructor.
     * @param LoggerInterface $logger
     * @param AsyncAccountGrav $accountGrav
     * @param AsyncAccountDoku $accountDoku
     * @param AsyncAccountPrettyNoemie $accountPrettyNoemie
     * @param CertTaskFactory $certTaskFactory
     * @param SiteFactory $siteFactory
     */
    public function __construct(LoggerInterface $logger, AsyncAccountGrav $accountGrav, AsyncAccountDoku $accountDoku, AsyncAccountPrettyNoemie $accountPrettyNoemie, CertTaskFactory $certTaskFactory, SiteFactory $siteFactory)
    {
        $this->accountGrav = $accountGrav;
        $this->accountDoku = $accountDoku;
        $this->accountPrettyNoemie = $accountPrettyNoemie;
        $this->certTaskFactory = $certTaskFactory;
        $this->siteFactory = $siteFactory;
        $this->logger = $logger;
    }

    /**
     * Page of a website
     *
     * @Route("/site/{site}", name="site-view", requirements={"site": "\d+"})
     * @param Site $site
     * @return Response
     */
    public function viewSiteAction(Site $site)
    {
        $this->checkAuthForSite($site);
        return $this->render('default/site/view.html.twig', ['site' => $site, 'buy_domain' => $this->getParameter('app.framasoft.buy_domains')]);
    }

    /**
     * @Route("/site/new", name="new-site")
     *
     * @return Response
     */
    public function viewChoiceAction(): Response
    {
        return $this->render('default/site/choice.html.twig', [
            'engines' => $this->getParameter('app.engines_enabled'),
        ]);
    }

    /**
     * Create a new blog
     *
     * @Route("/site/new/site/{type}", name="new-blog", defaults={"type": null})
     * @param Request $request
     * @param string|null $type
     * @return Response
     */
    public function newBlogAction(Request $request, string $type = 'site'): Response
    {
        $user = $this->getUser();
        /** @var ArrayCollection $blogs */
        $blogs = $user->getBlogs();

        $limit = $this->getParameter('app.blogs_limit');
        if ($blogs->count() >= $limit) {
            throw new AccessDeniedException("You already have " . $limit . " sites");
        }

        $site = new Blog();

        $siteType = 'site.module.' . $type;
        switch ($type) {
            case 'blog':
                $site->setSiteModules([Blog::MODULE_BLOG]);
                break;
            case 'cv':
                $site->setSiteModules([Blog::MODULE_CV]);
                break;
            case 'one_page':
                $site->setSiteModules([Blog::MODULE_SINGLE_PAGE]);
                break;
            default:
                $siteType = null;
                $site->setSiteModules([]);
        }

        $fs = new Finder();
        $languages = [];
        try {
            $engineLanguages = $fs->files()->in($this->getParameter('app.path_grav') . '/system/languages')->name(
                '*.yaml'
            );
            $langNames = Intl::getLanguageBundle()->getLanguageNames();
            foreach ($engineLanguages as $engineLanguage) {
                /** @var SplFileInfo $engineLanguage */
                $lang = substr($engineLanguage->getRelativePathname(), 0, -5);
                $languages[$langNames[$lang]] = $lang;
            }

            $site->setCreatedAt(new \DateTime())
                ->setUser($this->getUser());
            $newSiteForm = $this->createForm(BlogType::class, $site, ['lang_choices' => $languages, 'user' => $user]);

            $newSiteForm->handleRequest($request);

            if ($newSiteForm->isSubmitted() && $newSiteForm->isValid()) {
                $this->checkSubDomainAvailability($site->getSubdomain(), Blog::class);

                $this->accountGrav->setUser($this->getUser());
                $this->accountGrav->setLogger($this->get('logger'));
                /** @var AbstractSiteUser $siteUser */
                $siteUser = $site->getSiteUsers();
                $siteUser->setAdmin(true);
                $this->accountGrav->setSite($site)->setSiteUser($siteUser);

                $this->accountGrav->make();

                $em = $this->getDoctrine()->getManager();
                $em->persist($site);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans(
                        'flashes.site.created',
                        ['%site%' => $site->getSubdomain()],
                        'messages'
                    )
                );

                return $this->redirectToRoute('homepage');
            }

            return $this->render(
                'default/site/new/blog.html.twig',
                [
                    'form' => $newSiteForm->createView(),
                    'site_type' => $siteType,
                    'site_base_type' => 'site',
                    'show_modules' => empty($site->getSiteModules()),
                ]
            );
        } catch (\InvalidArgumentException $e) {
            $this->get('logger')->error("Directory " . $this->getParameter('app.path_grav') . '/system/languages' . " wasn't writable");
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans(
                    'flashes.site.no_template',
                    ['%site%' => $site->getSubdomain()],
                    'messages'
                )
            );

            return $this->redirectToRoute('homepage');
        } catch (SubDomainUnavailableException $e) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans(
                    'flashes.site.exists',
                    ['%site%' => $site->getSubdomain()],
                    'messages'
                )
            );
            return $this->redirectToRoute('new-blog');
        } catch (SiteCreationException $e) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans(
                    'flashes.site.error',
                    ['%site%' => $site->getSubdomain()],
                    'messages'
                )
            );
            return $this->redirectToRoute('new-blog');
        }
    }

    /**
     * Create a new Single page site
     *
     * @Route("/site/new/single-page", name="new-single-page")
     * @param Request $request
     * @return Response
     */
    public function newSinglePageAction(Request $request): Response
    {
        $user = $this->getUser();
        /** @var ArrayCollection $singlepages */
        $singlepages = $user->getSinglePages();

        $limit = $this->getParameter('app.single_page_limit');
        if ($singlepages->count() >= $limit) {
            throw new AccessDeniedException("You already have " . $limit . " single page websites");
        }

        $site = new SinglePage();
        $site->setCreatedAt(new \DateTime())
            ->setUser($this->getUser());
        $newSiteForm = $this->createForm(SinglePageType::class, $site, ['user' => $user]);

        $newSiteForm->handleRequest($request);

        if ($newSiteForm->isSubmitted() && $newSiteForm->isValid()) {
            try {
                $this->checkSubDomainAvailability($site->getSubdomain(), SinglePage::class);

                $this->accountPrettyNoemie->setUser($this->getUser());
                $this->accountPrettyNoemie->setLogger($this->get('logger'));
                $this->accountPrettyNoemie->setSite($site)->setSiteUser($site->getSiteUsers());
                $this->accountPrettyNoemie->make();

                $em = $this->getDoctrine()->getManager();
                $em->persist($site);
                $em->flush();
            } catch (SubDomainUnavailableException $e) {
                $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->get('translator')->trans('flashes.site.exists', ['%site%' => $site->getSubdomain()], 'messages')
                );
                return $this->redirectToRoute('new-single-page');
            } catch (SiteCreationException $e) {
                $this->logger->error("Error while creating the site single page " . $site->getSubdomain());
                return $this->redirectToRoute('new-single-page');
            } catch (SiteUserExistsException $e) {
                $this->logger->error("Site user already existed when creating site ? What a strange issue !");
            }

            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/site/new/single-page.html.twig', [
            'form' => $newSiteForm->createView(),
            'site_base_type' => 'single-page',
            'site_type' => $this->get('translator')->trans('site.type.single-page'),
        ]);
    }

    /**
     * Create a new Wiki
     *
     * @Route("/site/new/wiki", name="new-wiki")
     * @param Request $request
     * @return Response
     */
    public function newWikiAction(Request $request): Response
    {
        $user = $this->getUser();
        /** @var ArrayCollection $wikis */
        $wikis = $user->getWikis();

        $limit = $this->getParameter('app.wikis_limit');
        if ($wikis->count() >= $limit) {
            throw new AccessDeniedException("You already have " . $limit . " wikis");
        }

        $site = new Wiki();
        $site->setCreatedAt(new \DateTime())
            ->setUser($this->getUser());
        $newSiteForm = $this->createForm(WikiType::class, $site, ['user' => $user]);

        $newSiteForm->handleRequest($request);

        if ($newSiteForm->isSubmitted() && $newSiteForm->isValid()) {
            try {
                $this->checkSubDomainAvailability($site->getSubdomain(), Wiki::class);

                $this->accountDoku->setUser($this->getUser());
                $this->accountDoku->setLogger($this->get('logger'));
                $this->accountDoku->setSite($site)->setSiteUser($site->getSiteUsers());
                $this->accountDoku->make();

                $em = $this->getDoctrine()->getManager();
                $em->persist($site);
                $em->flush();
            } catch (SubDomainUnavailableException $e) {
                $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->get('translator')->trans('flashes.site.exists', ['%site%' => $site->getSubdomain()], 'messages')
                );
                return $this->redirectToRoute('new-wiki');
            } catch (SiteCreationException $e) {
                $this->logger->error("Error while creating the wiki " . $site->getSubdomain());
                return $this->redirectToRoute('new-wiki');
            }

            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/site/new/wiki.html.twig', [
            'form' => $newSiteForm->createView(),
            'site_base_type' => 'wiki',
            'site_type' => $this->get('translator')->trans('site.type.wiki'),
        ]);
    }

    /**
     * Get whether site subdomain is available
     *
     * @Route("/site/available", name="site-available")
     * @param Request $request
     * @return JsonResponse
     */
    public function getSubDomainAvailability(Request $request)
    {
        $domain = $request->get('domain');
        $type = $request->get('type');
        if ($type === 'site') {
            $type = Blog::class;
        } elseif ($type === 'single-page') {
            $type = SinglePage::class;
        } elseif ($type === 'wiki') {
            $type = Wiki::class;
        }

        try {
            $this->checkSubDomainAvailability($domain, $type);
        } catch (SubDomainUnavailableException $e) {
            return new JsonResponse(['status' => 'failure', 'message' => $e->getMessage(), 'reason' => $e->getReason()], 403);
        }
        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * Check subdomain availability
     *
     * We check multiple things here :
     * - If the subdomain is forbidden (things like www.frama.site and contact.frama.site)
     * - If the subdomain is already taken by another user
     * - If the subdomain is already used by the same user for a site of the same type
     * - If the subdomain is already used by the same user for a blog / single_page and she wants to create a single_page / blog
     *
     * @param string $subDomain
     * @param string $type
     * @return bool
     * @throws SubDomainUnavailableException
     */
    private function checkSubDomainAvailability(string $subDomain, string $type)
    {
        if ($type === Wiki::class) {
            $typeString = 'wiki';
        } elseif ($type === Blog::class || $type === SinglePage::class) {
            $typeString = 'site';
        } else {
            $typeString = null;
        }

        if (in_array($subDomain, Site::FORBIDDEN_SUBDOMAINS)) {
            throw new SubDomainUnavailableException(SubDomainUnavailableException::REASON_FORBIDDEN, $subDomain, $this->getUser(), $this->get('translator')->trans('site.new.subdomain.error', ['%subdomain%' => $subDomain, '%type%' => lcfirst($typeString)]));
        }

        $site = $this->getDoctrine()->getRepository('AppBundle:Site')->findOneBy(['subdomain' => $subDomain]);

        if (null != $site && # We already have a website for this subdomain
            (
                $site->getUser() !== $this->getUser() || # The user is different
                $this->checkSinglePageBlogCompatibility($site, $type) ||
                ($site->getUser() === $this->getUser() && $site instanceof $type) # We already have a site with this subdomain of the same type
            )
        ) {
            throw new SubDomainUnavailableException(SubDomainUnavailableException::REASON_TAKEN, $subDomain, $this->getUser(), $this->get('translator')->trans('site.new.subdomain.not_available', ['%subdomain%' => $subDomain, '%type%' => lcfirst($typeString)]));
        }

        return true;
    }

    /**
     * Checks if we can have blog and single pages
     *
     * We have a single page and we want to create a blog or we have a blog and we want to create a single page
     *
     * @param Site $site
     * @param string $type
     * @return bool
     */
    private function checkSinglePageBlogCompatibility(Site $site, string $type): bool
    {
        return ($site instanceof SinglePage && $type === Blog::class) || ($site instanceof Blog && $type === SinglePage::class);
    }

    /**
     * Check if we can have sites and wikis
     *
     * @param Site $site
     * @param string $typeString
     * @return bool
     */
    private function checkWikiSiteCompatibility(Site $site, string $typeString): bool
    {
        return ((($site instanceof Blog) || ($site instanceof SinglePage)) && $typeString === 'wiki') || (($site instanceof Wiki) && $typeString === 'site');
    }

    /**
     * Delete a website
     *
     * @Route("/site/{site}/delete", name="delete-site")
     * @param Request $request
     * @param Site $site
     * @return Response
     * @throws SiteDeletionException
     * @throws UnknownSiteType
     */
    public function deleteSiteAction(Request $request, Site $site)
    {
        $this->checkAuthForSite($site);
        $deleteSiteForm = $this->createForm(DeleteSiteForm::class);

        $deleteSiteForm->handleRequest($request);

        if ($deleteSiteForm->isSubmitted() && $deleteSiteForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /**
             * Let's start with resetting domains sites
             */
            foreach ($site->getDomains() as $domain) {
                $this->certTaskFactory->createCertTaskForAttachment($this->getUser(), $domain, CertData::ACTION_DELETE);
                /** @var $domain Domain */
                $domain->removeSite();
                $domain->setStatus(Domain::DOMAIN_DETACHED);
                $em->persist($domain);

                /**
                 * If the site is a wiki, we need to delete the symlinks
                 */
                if ($site instanceof Wiki) {
                    $this->accountDoku->setSite($site)->setUser($this->getUser())->setLogger($this->get('logger'));
                    $this->accountDoku->unlinkDomainFolder($domain->getDomainName());
                } elseif ($site instanceof SinglePage) {
                    $this->accountPrettyNoemie->setSite($site)->setUser($this->getUser())->setLogger($this->get('logger'));
                    $this->accountPrettyNoemie->unlinkDomainFolder($domain->getDomainName());
                }
            }

            /**
             * Delete files
             */
            if ($site instanceof Blog) {
                $this->accountGrav->setSite($site)->setLogger($this->get('logger'));
                $this->accountGrav->deleteSite();
            } elseif ($site instanceof Wiki) {
                $this->accountDoku->setSite($site)->setLogger($this->get('logger'));
                $this->accountDoku->deleteSite();
            }

            /**
             * Delete site in database
             */
            $em->remove($site);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('flashes.site.deleted', ['%site%' => $site->getSubdomain()], 'messages')
            );

            return $this->redirectToRoute('homepage');
        }

        $referer = $request->headers->get('referer') ?: $this->get('router')->generate('homepage', [], Router::ABSOLUTE_URL);

        return $this->render('default/site/delete.html.twig', [
            'form' => $deleteSiteForm->createView(),
            'site' => $site,
            'referer' => $referer,
        ]);
    }

    /**
     * Add an user to a website
     *
     * @Route("/site/{site}/user/add", name="site-add-user")
     * @param Request $request
     * @param Site $site
     * @return Response
     * @throws \Exception
     */
    public function addSiteUserAction(Request $request, Site $site): Response
    {
        $this->checkAuthForSite($site);
        if ($site->getUser() !== $this->getUser()) {
            throw new UnauthorizedHttpException("You can't edit this website");
        }

        if ($site instanceof Blog) {
            $siteUser = new BlogUser($site);

            $fs = new Finder();
            $languages = [];
            $engineLanguages = $fs->files()->in($this->getParameter('app.path_grav') . '/system/languages')->name('*.yaml');
            $langNames = Intl::getLanguageBundle()->getLanguageNames();
            foreach ($engineLanguages as $engineLanguage) {
                /** @var SplFileInfo $engineLanguage */
                $lang = substr($engineLanguage->getRelativePathname(), 0, -5);
                $languages[$langNames[$lang]] = $lang;
            }
            $userForm = $this->createForm(BlogUserType::class, $siteUser, ['lang_choices' => $languages, 'user' => $this->getUser()]);
        } elseif ($site instanceof Wiki) {
            $siteUser = new WikiUser($site);
            $userForm = $this->createForm(WikiUserType::class, $siteUser, ['user' => $this->getUser()]);
        } elseif ($site instanceof SinglePage) {
            $siteUser = new SinglePageUser();
            $userForm = $this->createForm(SinglePageUserType::class, $siteUser, ['user' => $this->getUser()]);
        } else {
            throw new UnknownSiteType(get_class($site),'Strange website');
        }

        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            if ($site instanceof Blog) {
                $accountFactory = $this->accountGrav;
            } elseif ($site instanceof SinglePage) {
                $accountFactory = $this->accountPrettyNoemie;
            } elseif ($site instanceof Wiki) {
                $accountFactory = $this->accountDoku;
            } else {
                return $this->render('default/site/user/add.html.twig', ['form' => $userForm->createView(), 'site' => $site]);
            }

            try {
                $accountFactory
                    ->setSite($site)
                    ->setSiteUser($siteUser)
                    ->setUser($this->getUser())
                    ->setLogger($this->get('logger'));
                $accountFactory->makeAccount();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans('flashes.site.user.added', ['%user%' => $siteUser], 'messages')
                );

                return $this->redirectToRoute('site-view', ['site' => $site->getId()]);
            } catch(SiteUserExistsException $e) {
                $this->logger->error("The user " . $e->getSiteUser() . " already exists for site " . $e->getSiteFolder());
                $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->get('translator')->trans('flashes.site.user.already_exists', ['%user%' => $siteUser], 'messages')
                );
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add(
                    'danger',
                    $this->get('translator')->trans('flashes.site.error', ['%user%' => $siteUser], 'messages')
                );
                return $this->render('default/site/user/add.html.twig', ['form' => $userForm->createView(), 'site' => $site]);
            }
        }

        return $this->render('default/site/user/add.html.twig', ['form' => $userForm->createView(), 'site' => $site]);
    }

    /**
     * Edit an user website
     *
     * @Route("/site/{site}/user/{username}/edit", name="site-edit-user")
     * @param Request $request
     * @param Site $site
     * @param string $username
     * @return Response
     */
    public function editSiteUserAction(Request $request, Site $site, string $username): Response
    {
        $this->checkAuthForSite($site);
        if ($site->getUser() !== $this->getUser()) {
            throw new UnauthorizedHttpException("You can't edit this website");
        }

        if ($site instanceof SinglePage) {
            $siteUser = new SinglePageUser($site);
            $siteUser->setUsername($username);
            $userForm = $this->createForm(SinglePageUserType::class, $siteUser, ['user' => $this->getUser()]);

            $userForm->handleRequest($request);

            if ($userForm->isSubmitted() && $userForm->isValid()) {
                if ($site instanceof Blog) {
                    $accountFactory = $this->accountGrav;
                } elseif ($site instanceof SinglePage) {
                    $accountFactory = $this->accountPrettyNoemie;
                } elseif ($site instanceof Wiki) {
                    $accountFactory = $this->accountDoku;
                } else {
                    return $this->redirectToRoute('site-view', ['site' => $site->getId()]);
                }

                try {
                    $accountFactory
                        ->setSite($site)
                        ->setSiteUser($siteUser)
                        ->setUser($this->getUser())
                        ->setLogger($this->get('logger'));
                    $accountFactory->editAccount($username);

                    $this->get('session')->getFlashBag()->add(
                        'success',
                        $this->get('translator')->trans('flashes.site.user.added', ['%user%' => $siteUser], 'messages')
                    );

                    return $this->redirectToRoute('site-view', ['site' => $site->getId()]);
                } catch (\Exception $e) {
                    $this->logger->error("An error occured", [$e->getMessage()]);
                    $this->get('session')->getFlashBag()->add(
                        'danger',
                        $this->get('translator')->trans('flashes.site.error', ['%user%' => $siteUser], 'messages')
                    );
                }
            }
            return $this->render('default/site/user/edit.html.twig', ['form' => $userForm->createView(), 'site' => $site]);
        } else {
            return $this->redirectToRoute('site-view', ['site' => $site->getId()]);
        }
    }

    /**
     * Remove an user from a website
     *
     * @Route("/site/{site}/user/remove/{siteUser}", name="delete-site-user")
     * @param Site $site
     * @param string $siteUser
     * @return RedirectResponse
     */
    public function removeSiteUserAction(Site $site, string $siteUser): RedirectResponse
    {
        $this->checkAuthForSite($site);

        try {
            if ($site instanceof Blog) {
                $accountFactory = $this->accountGrav;
            } elseif ($site instanceof SinglePage) {
                $accountFactory = $this->accountPrettyNoemie;
            } elseif ($site instanceof Wiki) {
                $accountFactory = $this->accountDoku;
            }

            /**
             * @var $accountFactory AsyncAccountInterface
             */
            $accountFactory
                ->setSite($site)
                ->setSiteUser($this->siteFactory->getUserForSite($site, $siteUser))
                ->setUser($this->getUser())
                ->setLogger($this->get('logger'));
            $accountFactory->deleteSiteUser();

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('flashes.site.user.deleted', ['%user%' => $siteUser], 'messages')
            );
        } catch (UserSiteDeletionException $e) {
            $this->get('logger')->error($e->getMessage());
        }

        return $this->redirectToRoute('site-view', ['site' => $site->getId()]);
    }

    /**
     * @param Site $command
     * @return bool
     */
    private function checkAuthForSite(Site $command)
    {
        if ($command->getUser() !== $this->getUser() && !($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('SUPER_ADMIN'))) {
            throw new AccessDeniedException("You don't have access to this website");
        }
        return true;
    }
}
