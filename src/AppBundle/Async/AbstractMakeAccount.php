<?php

namespace AppBundle\Async;

use AppBundle\Entity\AbstractSiteUser;
use AppBundle\Entity\Site;
use DirectoryIterator;
use Psr\Log\LoggerInterface;

abstract class AbstractMakeAccount extends AbstractAsync implements AsyncInterface
{

    /**
     * @var AbstractSiteUser
     */
    protected $siteUser;

    /**
     * @var Site
     */
    protected $site;

    /**
     * Path to the accounts folder
     *
     * @var string
     */
    protected $accountsPath;

    /**
     * @var string
     *
     * Path to Doku Engine Folder
     */
    protected $enginePath;

    public function __construct(LoggerInterface $logger, string $accountsPath, string $enginePath)
    {
        parent::__construct($logger);
        $this->accountsPath = $accountsPath;
        $this->enginePath = $enginePath;
    }

    /**
     * @return Site
     */
    public function getSite(): Site
    {
        return $this->site;
    }

    /**
     * @param Site $site
     * @return AbstractMakeAccount
     */
    public function setSite(Site $site): AbstractMakeAccount
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return AbstractSiteUser
     */
    public function getSiteUser(): AbstractSiteUser
    {
        return $this->siteUser;
    }

    /**
     * @param AbstractSiteUser $siteUser
     * @return AbstractMakeAccount
     */
    public function setSiteUser(AbstractSiteUser $siteUser): AbstractMakeAccount
    {
        $this->siteUser = $siteUser;
        return $this;
    }

    /**
     * Recursively copy files from one directory to another
     *
     * @param String $src - Source of files being moved
     * @param String $dest - Destination of files being moved
     * @return bool
     */
    protected function rcopy(string $src, string $dest): bool
    {

        // If source is not a directory stop processing
        if (!is_dir($src)) {
            return false;
        }

        // If the destination directory does not exist create it
        if (!is_dir($dest) && !mkdir($dest)) {
            // If the destination directory could not be created stop processing
            return false;
        }

        // Open the source directory to read in files
        $i = new DirectoryIterator($src);
        foreach ($i as $f) {
            if ($f->isFile()) {
                copy($f->getRealPath(), "$dest/" . $f->getFilename());
            } elseif (!$f->isDot() && $f->isDir()) {
                $this->rcopy($f->getRealPath(), "$dest/$f");
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function listSites()
    {
        return array_diff(scandir($this->accountsPath), array('..', '.'));
    }

    /**
     * @param string $value
     * @return bool
     */
    public function isValidString(string $value = null): bool
    {
        return $value != null && is_string($value) && $value != '';
    }
}
