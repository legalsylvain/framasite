<?php

namespace UserBundle\Controller;

use AppBundle\Async\AsyncAccountDoku;
use AppBundle\Async\AsyncAccountGrav;
use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Certificate\CertData;
use AppBundle\Entity\Domain;
use AppBundle\Entity\Site;
use AppBundle\Entity\User;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Exception\APIException;
use AppBundle\Helper\CertTaskFactory;
use AppBundle\Helper\DomainFactory;
use AppBundle\Helper\UserFactory;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\OutOfRangeCurrentPageException;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Form\NewUserType;
use UserBundle\Form\SearchUserType;
use UserBundle\Form\UserType;

/**
 * User controller.
 */
class ManageController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AsyncAccountGrav
     */
    private $accountGrav;

    /**
     * @var AsyncAccountDoku
     */
    private $accountDoku;

    /**
     * @var CertTaskFactory
     */
    private $certTaskFactory;

    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * ManageController constructor.
     * @param EntityManagerInterface $entityManager
     * @param AsyncAccountGrav $accountGrav
     * @param AsyncAccountDoku $accountDoku
     * @param CertTaskFactory $certTaskFactory
     * @param DomainFactory $domainFactory
     * @param UserFactory $userFactory
     */
    public function __construct(EntityManagerInterface $entityManager, AsyncAccountGrav $accountGrav, AsyncAccountDoku $accountDoku, CertTaskFactory $certTaskFactory, DomainFactory $domainFactory, UserFactory $userFactory)
    {
        $this->entityManager = $entityManager;
        $this->accountGrav = $accountGrav;
        $this->accountDoku = $accountDoku;
        $this->certTaskFactory = $certTaskFactory;
        $this->domainFactory = $domainFactory;
        $this->userFactory = $userFactory;
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        // enable created user by default
        $user->setEnabled(true);

        $form = $this->createForm(NewUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->updateUser($user);

            // dispatch a created event so the associated config will be created
            $event = new UserEvent($user, $request);
            $this->get('event_dispatcher')->dispatch(FOSUserEvents::USER_CREATED, $event);

            $this->get('session')->getFlashBag()->add(
                'notice',
                $this->get('translator')->trans('flashes.user.notice.added', ['%username%' => $user->getUsername()], 'messages')
            );

            return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
        }

        return $this->render('default/Manage/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_view", requirements={"id": "\d+"})
     *
     * @param User $user
     * @return Response
     */
    public function viewAction(User $user): Response
    {
        return $this->render('default/Manage/view.html.twig', ['user' => $user]);
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param User $user
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm(UserType::class, $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                $this->get('translator')->trans('flashes.user.notice.updated', ['%username%' => $user->getUsername()], 'messages')
            );

            return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
        }

        return $this->render('default/Manage/edit.html.twig', [
            'user' => $user,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/delete/{id}", name="user_delete")
     * @param User $user
     * @return Response
     */
    public function deleteAction(User $user): Response
    {
        try {
            $this->userFactory->deleteDependencies($user);

            $this->get('session')->getFlashBag()->add(
                'notice',
                $this->get('translator')->trans('flashes.user.deleted.success', ['%username%' => $user->getUsername()], 'messages')
            );
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('flashes.user.deleted.error', ['%username%' => $user->getUsername(), '%error%' => $e->getMessage()], 'messages')
            );
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('user_index');
    }

    /**
     * @param Request $request
     * @param int     $page
     *
     * @Route("/list/{page}", name="user_index", defaults={"page" = 1})
     *
     * Default parameter for page is hardcoded (in duplication of the defaults from the Route)
     * because this controller is also called inside the layout template without any page as argument
     *
     * @return Response
     */
    public function searchFormAction(Request $request, $page = 1)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('AppBundle:User')->createQueryBuilder('u');

        $form = $this->createForm(SearchUserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('logger')->info('searching users');

            $searchTerm = (isset($request->get('search_user')['term']) ? $request->get('search_user')['term'] : '');

            $qb = $em->getRepository('AppBundle:User')->getQueryBuilderForSearch($searchTerm);
        }

        $pagerAdapter = new DoctrineORMAdapter($qb->getQuery(), true, false);
        $pagerFanta = new Pagerfanta($pagerAdapter);
        $pagerFanta->setMaxPerPage(50);

        try {
            $pagerFanta->setCurrentPage($page);
        } catch (OutOfRangeCurrentPageException $e) {
            if ($page > 1) {
                return $this->redirect($this->generateUrl('user_index', ['page' => $pagerFanta->getNbPages()]), 302);
            }
        }

        return $this->render('default/Manage/index.html.twig', [
            'searchForm' => $form->createView(),
            'users' => $pagerFanta,
        ]);
    }
}
