<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Controller\BuyDomains\DomainCommandController;
use AppBundle\Exception\APIException;
use AppBundle\Exception\LimitationException;
use Tests\AppBundle\FramasitesTestCase;

class DomainControllerTest extends FramasitesTestCase
{
    public function testIndexDomains()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/domain');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertCount(1, $crawler->filter('tbody tr'));
        $this->assertEquals($this->getEntityManager()->getRepository('AppBundle:Domain')->findOneBy(['user' => $this->getLoggedInUser()])->getDomainName(), $crawler->filter('tbody tr:first-child td:first-child a')->extract(['_text'])[0]);

        $this->assertContains('domain.list.assistant.title', $crawler->filter('h2')->extract(['_text'])[1]);
    }

    public function testViewHelp()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/domain/help');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("C'est quoi ?", $crawler->filter('body')->extract(['_text'])[0]);
    }

    public function dataForSuggestedDomains()
    {
        return [
            [
                'domaintest1',
                sizeof(DomainCommandController::OFFERED_EXTENSIONS),
                null,
            ],
            [
                'domaintest2.fr',
                1,
                null,
            ],
            [
                'domaintest3.no',
                0,
                LimitationException::class
            ],
            [
                '..',
                0,
                LimitationException::class,
            ],
            [
                'undefined',
                0,
                \Exception::class,
            ]
        ];
    }

    /**
     * @dataProvider dataForSuggestedDomains
     * @param string $domainToTest
     * @param int $nbResults
     * @param string $exception
     */
    public function testGetSuggestedDomains(string $domainToTest, int $nbResults, string $exception = null)
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $client->request('GET', '/domain/suggest/' . $domainToTest);
        if (null === $exception) {
            $this->assertEquals(200, $client->getResponse()->getStatusCode());
            $data = json_decode($client->getResponse()->getContent(), true);
            $this->assertEquals($nbResults, sizeof($data));
        } elseif (LimitationException::class === $exception) {
            $this->assertEquals(401, $client->getResponse()->getStatusCode());
        } elseif (APIException::class === $exception) {
            $this->assertEquals(400, $client->getResponse()->getStatusCode());
        } else {
            $this->assertEquals(400, $client->getResponse()->getStatusCode());
            $this->assertContains("Can\u0027t check undefined. Fix your shit", $client->getResponse()->getContent());
        }
    }

    public function testAttachDomain()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/domain/attach');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('domain.attach.title', $crawler->filter('h2')->extract(['_text'])[0]);

        $form = $crawler->filter('button[id=attach_domain_save]')->form();
        $data = [
            'attach_domain[domainName]' => 'demo.com',
            'attach_domain[site]' => '1',
        ];
        $crawler = $client->submit($form, $data);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('domain.incorrect', $crawler->filter('body')->extract(['_text'])[0]);
    }
}