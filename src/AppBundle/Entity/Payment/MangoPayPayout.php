<?php

namespace AppBundle\Entity\Payment;

use Symfony\Component\Validator\Constraints as Assert;

class MangoPayPayout
{

    /**
     * @var string
     *
     */
    private $authorId;

    /**
     * @var float
     *
     * @Assert\NotBlank()
     */
    private $debitedFunds;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $bankAccountId;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    private $debitedWalletId;

    /**
     * @var string
     *
     * @Assert\Length(max="12")
     */
    private $reference;

    /**
     * @return string
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * @param string $authorId
     * @return MangoPayPayout
     */
    public function setAuthorId(string $authorId): MangoPayPayout
    {
        $this->authorId = $authorId;
        return $this;
    }

    /**
     * @return float
     */
    public function getDebitedFunds()
    {
        return $this->debitedFunds;
    }

    /**
     * @param float $debitedFunds
     * @return MangoPayPayout
     */
    public function setDebitedFunds(float $debitedFunds): MangoPayPayout
    {
        $this->debitedFunds = $debitedFunds;
        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccountId()
    {
        return $this->bankAccountId;
    }

    /**
     * @param string $bankAccountId
     * @return MangoPayPayout
     */
    public function setBankAccountId(string $bankAccountId): MangoPayPayout
    {
        $this->bankAccountId = $bankAccountId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDebitedWalletId()
    {
        return $this->debitedWalletId;
    }

    /**
     * @param string $debitedWalletId
     * @return MangoPayPayout
     */
    public function setDebitedWalletId(string $debitedWalletId): MangoPayPayout
    {
        $this->debitedWalletId = $debitedWalletId;
        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return MangoPayPayout
     */
    public function setReference(string $reference = null): MangoPayPayout
    {
        $this->reference = $reference;
        return $this;
    }
}
