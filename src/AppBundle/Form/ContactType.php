<?php

namespace AppBundle\Form;

use AppBundle\Entity\Contact;
use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $options['user'];
        $builder
            ->add('email', EmailType::class, [
                'label' => 'contact.email.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['placeholder' => 'contact.email.placeholder', 'autocomplete' => 'email'],
                'data' => $user->getEmail(),
            ])
            ->add('password', TextType::class, [
                'label' => 'contact.password.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['readonly' => true],
            ])
            ->add('givenName', TextType::class, [
                'label' => 'contact.givenName.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['placeholder' => 'contact.givenName.placeholder', 'autocomplete' => 'given-name'],
                'data' => $user->getFirstName(),
            ])
            ->add('familyName', TextType::class, [
                'label' => 'contact.familyName.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['placeholder' => 'contact.familyName.placeholder', 'autocomplete' => 'family-name'],
                'data' => $user->getLastName(),
            ])
            ->add('streetAddress', TextareaType::class, [
                'label' => 'contact.streetAddress.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['placeholder' => 'contact.streetAddress.placeholder', 'autocomplete' => 'street-address'],
                'data' => $user->getStreetAddress(),
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'contact.zipCode.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['placeholder' => 'contact.zipCode.placeholder', 'autocomplete' => 'postal-code'],
                'data' => $user->getZipCode(),
            ])
            ->add('city', TextType::class, [
                'label' => 'contact.city.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['placeholder' => 'contact.city.placeholder', 'autocomplete' => 'address-level2'],
                'data' => $user->getCity(),
            ])
            ->add('country', CountryType::class, [
                'label' => 'contact.country.label',
                'preferred_choices' => array('FR', 'GB', 'US', 'DE'),
                'data' => $user->getCountry(),
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['autocomplete' => 'country']
            ])
            ->add('phone', TextType::class, [
                'label' => 'contact.phone.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['placeholder' => 'contact.phone.placeholder', 'autocomplete' => 'tel'],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'contact.type.private' => Contact::TYPE_PRIVATE,
                    'contact.type.company' => Contact::TYPE_COMPANY,
                    'contact.type.association' => Contact::TYPE_ASSOCIATION,
                    'contact.type.public_body' => Contact::TYPE_PUBLIC_BODY,
                ],
                'empty_data' => (string) Contact::TYPE_PRIVATE,
                'expanded' => true,
                'multiple' => false,
                'label' => 'contact.type.label',
                'label_attr' => ['class' => 'col-sm-3 radio-inline'],
            ])
            ->add('obfuscateInfo', CheckboxType::class, [
                'label' => 'contact.obfuscate',
                'label_attr' => ['class' => 'col-sm-3'],
            ])
            ->add('orgName', TextType::class, [
                'label' => 'contact.orgname.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['placeholder' => 'contact.orgname.placeholder', 'autocomplete' => 'organization'],
                'required' => false,
                'data' => $user->getLegalName(),
            ])
            ->add('save', SubmitType::class, [
                'label' => 'contact.save',
                'attr' => ['class' => 'btn btn-success'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'email' => null,
            'user' => null,
            'data_class' => Contact::class,
        ]);
    }
}
