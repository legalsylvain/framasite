<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Site;
use AppBundle\Entity\User;
use AppBundle\Exception\UserDeletionException;
use AppBundle\Form\DeleteAccountForm;
use AppBundle\Helper\SiteFactory;
use AppBundle\Helper\UserFactory;
use DirectoryIterator;
use FOS\UserBundle\Model\UserManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Translation\TranslatorInterface;

class DefaultController extends Controller
{
    /**
     * @var SiteFactory
     */
    private $siteFactory;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * DefaultController constructor.
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     * @param FlashBagInterface $flashBag
     * @param SiteFactory $siteFactory
     */
    public function __construct(LoggerInterface $logger, TranslatorInterface $translator, FlashBagInterface $flashBag, SiteFactory $siteFactory)
    {
        $this->siteFactory = $siteFactory;
        $this->logger = $logger;
        $this->translator = $translator;
        $this->flashBag = $flashBag;
    }

    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function indexAction(): Response
    {
        if ($this->getUser()) {
            $sites = $this->getUser()->getSites();
            foreach ($sites as $site) {
                /** @var $site Site */
                $site->setSiteUsers($this->siteFactory->getUsersForSite($site));
            }

            return $this->render('default/panel.html.twig', [
                'sites' => $sites,
                'engines' => $this->getParameter('app.engines_enabled'),
                'buy_domains' => $this->getParameter('app.framasoft.buy_domains'),
            ]);
        }

        $nbAccountsBlog = iterator_count(new DirectoryIterator($this->getParameter('kernel.project_dir').'/blog/users'))-2;
        $nbAccountsWiki = iterator_count(new DirectoryIterator($this->getParameter('kernel.project_dir').'/wiki/users'))-2;

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'error' => false,
            'csrf_token' => false,
            'last_username' => '',
            'nbAccounts' => $nbAccountsBlog + $nbAccountsWiki,
            'nbAccountsBlog' => $nbAccountsBlog,
            'nbAccountsWiki' => $nbAccountsWiki,
            'buy_domains' => $this->getParameter('app.framasoft.buy_domains'),
            'registration_enabled' => $this->getParameter('app.registration.enabled'),
        ]);
    }

    /**
     * @Route("/stats", name="stats")
     *
     * @return JsonResponse
     */
    public function statsAction(): JsonResponse
    {
        $cache = new FilesystemAdapter();
        $cachedStats = $cache->getItem('stats');
        $cachedStats->expiresAfter(\DateInterval::createFromDateString('2 hours'));
        if (!$cachedStats->isHit()) {
            $nbUsers = $this->getDoctrine()->getRepository('AppBundle:User')->getSumEnabledUsers();
            $nbAccountsBlog = iterator_count(
                    new DirectoryIterator($this->getParameter('kernel.project_dir') . '/blog/users')
                ) - 2;
            $nbAccountsWiki = iterator_count(
                    new DirectoryIterator($this->getParameter('kernel.project_dir') . '/wiki/users')
                ) - 2;
            $domainRepo = $this->getDoctrine()->getRepository('AppBundle:Domain');
            $totalDomains = $domainRepo->getSumActiveDomains();
            $totalDomainsBoughtThroughFrama = $domainRepo->getSumActiveDomainsByFrama();

            $stats = [
                'users' => (int)$nbUsers,
                'sites' => (int)$nbAccountsBlog,
                'wikis' => (int)$nbAccountsWiki,
                'domains' => [
                    'total' => (int)$totalDomains,
                    'bought_by_frama' => (int)$totalDomainsBoughtThroughFrama,
                ],
            ];
            $cachedStats->set($stats);
            $cache->save($cachedStats);
        }
        return new JsonResponse($cachedStats->get());
    }

    /**
     * @Route("/about/cgu", name="stripe-cgu")
     *
     * @return Response
     */
    public function viewCGUAction(): Response
    {
        return $this->file('Mangopay_Terms-FR.pdf', 'Mangopay_Terms-FR.pdf', ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route("/register/disabled", name="registration-disabled")
     *
     * @return Response
     */
    public function disabledRegistrationAction(): Response
    {
        return $this->render('default/registration-disabled.html.twig');
    }

    /**
     * @Route("/bye", name="delete-all-data")
     *
     * @param Request $request
     * @param UserFactory $userFactory
     * @param UserManagerInterface $userManager
     * @return Response
     */
    public function deleteAllDataAction(Request $request, UserFactory $userFactory, UserManagerInterface $userManager): Response
    {
        $deleteDataForm = $this->createForm(DeleteAccountForm::class);
        $deleteDataForm->handleRequest($request);

        /** @var User $user */
        $user = $this->getUser();
        $username = $user->getUsername();

        if ($deleteDataForm->isSubmitted() && $deleteDataForm->isValid()) {
            try {
                $this->logger->info('Trying to delete all of ' . $user->getUsername() . "'s sites and domains");
                $userFactory->deleteDependencies($user);

                $this->logger->info('Deleting user itself');
                $userManager->deleteUser($user);

                $this->flashBag->add(
                    'notice',
                    $this->translator->trans('flashes.user.deleted.success', ['%username%' => $username], 'messages')
                );
            } catch (UserDeletionException $e) {
                $this->logger->error('Exception while deleting user ' . $user->getUsername() . ' : ' . $e->getMessage());
                $this->flashBag->add(
                    'danger',
                    $this->translator->trans('flashes.user.deleted.error', ['%username%' => $username, '%error%' => $e->getMessage()], 'messages')
                );
            }
            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/delete_account.html.twig', ['form' => $deleteDataForm->createView()]);
    }
}
