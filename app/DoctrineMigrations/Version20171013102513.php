<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171013102513 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE framasite_user ADD type INT NOT NULL DEFAULT 1');
        $this->addSql('ALTER TABLE framasite_user ADD legal_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE framasite_user ADD birthday TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE framasite_user DROP type');
        $this->addSql('ALTER TABLE framasite_user DROP legal_name');
        $this->addSql('ALTER TABLE framasite_user DROP birthday');
    }
}
