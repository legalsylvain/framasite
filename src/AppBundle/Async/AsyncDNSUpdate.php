<?php

namespace AppBundle\Async;

use AppBundle\Entity\Domain;
use AppBundle\Entity\Zone\Record;
use AppBundle\Entity\Zone\Version;
use AppBundle\Entity\Zone\Zone;
use AppBundle\Helper\ZoneFactory;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

class AsyncDNSUpdate extends AbstractAsync implements AsyncInterface
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var int
     */
    private $defaultZone;

    /**
     * @var ZoneFactory
     */
    private $zoneFactory;

    /**
     * AsyncDNSUpdate constructor.
     * @param LoggerInterface $logger
     * @param EntityManager $em
     * @param ZoneFactory $zoneFactory
     * @param $defaultZone
     */
    public function __construct(LoggerInterface $logger, EntityManager $em, ZoneFactory $zoneFactory, $defaultZone)
    {
        parent::__construct($logger);
        $this->em = $em;
        $this->zoneFactory = $zoneFactory;
        $this->defaultZone = $defaultZone;
    }

    /**
     * @param Zone $zone
     * @param Domain $domain
     */
    public function setZoneForDomain(Zone $zone, Domain $domain)
    {
        $this->zoneFactory->setZoneForDomain($zone, $domain);

        $domain->setStatus(Domain::DOMAIN_DNS_CHANGED);
        $domain->setDnsZone($zone->getId());
        $this->em->persist($domain);
        $this->em->flush();
    }

    /**
     * @param Domain $domain
     */
    public function setDefaultZoneForDomain(Domain $domain)
    {
        $zone = $this->zoneFactory->cloneZone(new Zone($this->defaultZone), 0, $domain->getDomainName());
        $this->setZoneForDomain($zone, $domain);
    }

    /**
     * @param Domain $domain
     */
    public function createZoneForDomain(Domain $domain)
    {
        /**
         * Creating a new zone
         * TODO: Check if we can clone a default one
         */
        $zone = new Zone();
        $zone->setName($domain->getDomainName());
        $zone = $this->zoneFactory->createZone($zone);
        $version = new Version($zone);
        $version = $this->zoneFactory->createVersion($version);

        // Record A
        $a = new Record();
        $a->setName('@')->setType('A')->setTtl(10800)->setValue(dns_get_record('frama.site', DNS_A)[0]['ip']);

        $this->zoneFactory->addRecord($a, $version);

        // Record AAAA
        $aaaa = new Record();
        $aaaa->setName('@')->setType('AAAA')->setTtl(10800)->setValue(dns_get_record('frama.site', DNS_AAAA)[0]['ipv6']);

        $this->zoneFactory->addRecord($aaaa, $version);

        // Record MX
        $mx1 = new Record();
        $mx1->setName('@')->setType('MX')->setTtl(10800)->setValue('50 fb.mail.gandi.net.');

        $mx2 = new Record();
        $mx2->setName('@')->setType('MX')->setTtl(10800)->setValue('10 spool.mail.gandi.net.');

        $this->zoneFactory->addRecord($mx1, $version);
        $this->zoneFactory->addRecord($mx2, $version);

        $this->zoneFactory->setZoneVersionActive($version);

        $this->zoneFactory->setZoneForDomain($zone, $domain);
    }
}
