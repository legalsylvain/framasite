<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Async\AsyncAccountDoku;
use AppBundle\Async\AsyncAccountGrav;
use AppBundle\Entity\Site;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConfigurationSiteValidator extends ConstraintValidator
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var AsyncAccountDoku
     */
    private $accountDoku;

    /**
     * @var AsyncAccountGrav
     */
    private $accountGrav;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(LoggerInterface $logger, AsyncAccountGrav $accountGrav, AsyncAccountDoku $accountDoku, EntityManagerInterface $entityManager)
    {
        $this->logger = $logger;
        $this->accountDoku = $accountDoku;
        $this->accountGrav = $accountGrav;
        $this->entityManager = $entityManager;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param Site $siteObject
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($siteObject, Constraint $constraint)
    {
        $subdomain = $siteObject->getSubdomain();
        $type = get_class($siteObject);

        $site = $this->entityManager->getRepository('AppBundle:Site')->findOneBy(['subdomain' => $subdomain]);

        if (in_array($subdomain, Site::FORBIDDEN_SUBDOMAINS)) {
            $this->context->buildViolation($constraint->alreadyExistsSiteMessage)
                ->addViolation();
        }

        if ((null != $site) && // if we found a site with the same subdomain
            (
                ($site->getUser() !== $siteObject->getUser()) || // and if the owner is not the same
                ($site->getUser() === $siteObject->getUser() && $site instanceof $type) // or it's the same but he already has a site of this type
            )
        ) {
            $this->context->buildViolation($constraint->alreadyExistsSiteMessage)
                ->addViolation();
        }
    }
}
